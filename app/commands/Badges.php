<?php

ini_set('memory_limit', '-1');

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Jenssegers\Date\Date;

class Badges extends Command {
  /**
   * The console command name.
   *
   * @var string
   */
  protected $name = 'ripper:badges';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description.';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  private $ripped = array();

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function fire()
  {
    $action = $this->argument('action');

    switch($action){
      case 'searchString':
        $this->searchString();
        break;

    }
  }

  private function searchString() {
    $badges = DB::table('habbo_badges')->get();

    foreach($badges as $badge){
      $searchString = array();
      
      foreach($badge['name'] as $name){
        $searchString[] = $name;
      }
      
      foreach($badge['description'] as $description){
        $searchString[] = $description;
      }
      
      $searchString[] = $badge['code'];
      DB::table('habbo_badges')
      ->where('code', '=', $badge['code'])
      ->update(array('search' => implode(', ', $searchString)));
      
      echo 'Update '.$badge['code']."\n";
    }
  }

  /**
   * Get the console command arguments.
   *
   * @return array
   */
  protected function getArguments()
  {
    return array(
        array('action', InputArgument::REQUIRED, '')
    );
  }

  /**
   * Get the console command options.
   *
   * @return array
   */
  protected function getOptions()
  {
    return array(
        array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
    );
  }
}
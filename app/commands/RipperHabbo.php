<?php

ini_set('memory_limit', '-1');

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Jenssegers\Date\Date;

class RipperHabbo extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ripper:habbo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private $ripped = array();

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $action = $this->argument('action');
        $name = $this->argument('habbo');
        $tld = $this->argument('tld');

        if($tld == ''){
            $tld = 'de';
        }else if($tld == 'com'){
            $tld = 'us';
        }

        switch ($action) {
            case 'rip':
                if(!$this->jobRunning($this->name)) {
                    $this->rip();
                }
                break;
            case 'ripHabbo':
                $this->ripHabbo($name, 2, 'hh'.$tld);
                break;
            case 'analy':
                $this->analyHabbos();
                break;
            case 'sign':
                $this->sign();
                break;
            case 'scan':
                $this->scan();
                break;
            case 'old_scan':
                $this->oldScan();
                break;
            case 'badges':
                $this->renewBadge();
                break;
            case 'renew':
                $habbos = Habbo::where('last_rip', '<=', Date::now()->sub('5 days')->format('Y-m-d H:i:s'))
                    ->where('name', '!=', '')
                    ->where('tld', '!=', 0)
                    ->where('tld', '!=', '')
                    ->take(20)
                    ->get();
                foreach($habbos as $habbo ) {
                    $this->ripHabbo($habbo->name, 1, $habbo->tld);
                }

                break;

        }
    }

    private function renewBadge() {
        $hotels = [
            'de', 'com', 'tr'
        ];

        foreach($hotels as $domain) {
            $json = $this->request('http://api.habbos.redbeed.de/api/habbos/list/badges/desc/00/10/' . $domain, '');//file_get_contents('http://api.habbos.redbeed.de/api/habbos/list/badges/desc/00/10/' . $domain);
            $json = json_decode($json);

            foreach ($json as $habbo) {
                try {
                    $tld = $domain;
                    if($domain == 'com'){
                        $tld = 'us';
                    }
                    $this->ripHabbo($habbo->name, 2, 'hh' . $tld);
                }catch (Exception $e){
                    continue;
                }catch (\Exception $e){
                    continue;
                }
            }
        }

        foreach($hotels as $domain){
            $json = $this->request('http://api.habbos.redbeed.de/api/habbos/list/badges/desc/10/90/'.$domain, '');//file_get_contents('http://api.habbos.redbeed.de/api/habbos/list/badges/desc/10/90/'.$domain);
            $json = json_decode($json);

            foreach( $json as $habbo){
                try {
                    $this->ripHabbo($habbo->name, 2, 'hh' . $domain);
                }catch (Exception $e){
                    continue;
                }catch (\Exception $e){
                    continue;
                }
            }
        }
    }

    private function scan()
    {
        /*getPromos*/
        $context = $this->getCookie();
        $promos = $this->request('https://www.habbo.com/api/public/promos', $context);//file_get_contents('https://www.habbo.com/api/public/promos', false, $context);
        $promos = json_decode($promos, true);
        $list = array();
        foreach($promos as $promo){
            if(isset($promo['imageUrl'])) {

                $check = $habbos = DB::connection('mongodb')
                    ->table('habbo_images')
                    ->where('src', '=', $promo['imageUrl'])
                    ->count();

                if($check == 0) {

                    echo  $promo['imageUrl']."\n";
                    $search = array();
                    $search[] = $promo['imageUrl'];
                    $search[] = $promo['title'];
                    $search[] = $promo['linkLabel'];

                    $image = array(
                        'src' => $promo['imageUrl'],
                        'search' => implode(' - ', $search),
                        "dt" => new MongoDate(strtotime(date("Y-m-d H:i:s")))
                    );

                    $habbos = DB::connection('mongodb')
                        ->table('habbo_images')
                        ->insert($image);
                }
            }
        }

        /*get Channels*/
        $context = $this->getCookie();
        $channels = $this->request('https://www.habbo.com/stories-api/public/channels', $context);//file_get_contents('https://www.habbo.com/stories-api/public/channels', false, $context);
        $channels = json_decode($channels, true);
        $list = array();
        foreach($channels as $channel){
            if(isset($channel['assetUrl'])) {

                $previewImage = $channel['assetUrl'].'preview.png';
                $contents = file_get_contents($previewImage);

                if (strlen($contents)){
                    $check = DB::connection('mongodb')
                        ->table('habbo_images')
                        ->where('src', '=', $previewImage)
                        ->count();

                    if ($check == 0) {

                        echo $previewImage . "\n";
                        $search = array();
                        $search[] = $previewImage;
                        $search[] = $channel['description'];
                        $search[] = implode(' - ', $channel['tags']);
                        if(isset($channel['title']))
                            $search[] = $channel['title'];
                        $search[] = $channel['id'];

                        $image = array(
                            'src' => $previewImage,
                            'search' => implode(' - ', $search),
                            "dt" => new MongoDate(strtotime(date("Y-m-d H:i:s")))
                        );

                        DB::connection('mongodb')
                            ->table('habbo_images')
                            ->insert($image);
                    }
                }
            }
        }

        /*get Inventrory*/

        $langs = array('en');

        foreach($langs as $lang) {
            $context = $this->getCookie();
            $inventory = $this->request('https://www.habbo.com/shopapi/public/inventory/'.$lang, $context);//file_get_contents('https://www.habbo.com/shopapi/public/inventory/'.$lang, false, $context);
            $inventory = json_decode($inventory, true);
            $list = array();
            foreach ($inventory['pricePoints'] as $price) {
                if (isset($price['paymentMethods'])) {

                    foreach ($price['paymentMethods'] as $method) {

                        /* Button Img */
                        $buttonLogoUrl = $method['buttonLogoUrl'];
                        if($buttonLogoUrl{0} == '/'){
                            $buttonLogoUrl = 'http:'.$buttonLogoUrl;
                        }
                        $contents = file_get_contents($buttonLogoUrl);

                        if (strlen($contents)){

                            $check = DB::connection('mongodb')
                                ->table('habbo_images')
                                ->where('src', '=', $buttonLogoUrl)
                                ->count();

                            if($check == 0) {

                                echo $buttonLogoUrl . "\n";

                                $search = array();
                                $search[] = $buttonLogoUrl;
                                $search[] = $method['category'];
                                if (isset($price['name']))
                                    $search[] = $price['name'];

                                $image = array(
                                    'src' => $buttonLogoUrl,
                                    'search' => implode(' - ', $search),
                                    "dt" => new MongoDate(strtotime(date("Y-m-d H:i:s")))
                                );

                                DB::connection('mongodb')
                                    ->table('habbo_images')
                                    ->insert($image);
                            }

                        }

                        if(isset($method['smallPrint']) && !empty($method['smallPrint'])){

                            $doc = new DOMDocument();
                            $doc->loadHTML($method['smallPrint']);
                            $imageTags = $doc->getElementsByTagName('img');

                            foreach($imageTags as $tag) {

                                $buttonLogoUrl = (string)$tag->getAttribute('src');
                                if ($buttonLogoUrl{0} == '/') {
                                    $buttonLogoUrl = 'http:' . $buttonLogoUrl;
                                }
                                $check = DB::connection('mongodb')
                                    ->table('habbo_images')
                                    ->where('src', '=', $buttonLogoUrl)
                                    ->count();

                                if($check == 0) {
                                    $contents = file_get_contents($buttonLogoUrl);

                                    if (strlen($contents)) {

                                        echo $buttonLogoUrl . "\n";

                                        $search = array();
                                        $search[] = $buttonLogoUrl;
                                        $search[] = $method['category'];
                                        if (isset($price['name']))
                                            $search[] = $price['name'];

                                        $image = array(
                                            'src' => $buttonLogoUrl,
                                            'search' => implode(' - ', $search),
                                            "dt" => new MongoDate(strtotime(date("Y-m-d H:i:s")))
                                        );

                                        DB::connection('mongodb')
                                            ->table('habbo_images')
                                            ->insert($image);
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
    }

    private function oldScan () {
        $habbos = [
            'http://www.habbo.de/',
            'http://www.habbo.fi/',
            'http://www.habbo.com.tr/',
            'http://www.habbo.ch/',
            'http://www.habbo.at/',
            'http://www.habbo.es/',
            'http://www.habbo.com.br/',
            'http://www.habbo.it/',
        ];

        foreach($habbos as $baseUrl){

            echo "\n"."\n"."\n".$baseUrl."\n";

            $html = new \Htmldom($baseUrl.'community');

            $news = $html->find('.promo-container');

            foreach($news as $conatiner){

                $style = $conatiner->style;
                preg_match_all('~\bbackground(-image)?\s*:(.*?)\(\s*(\'|")?(?<image>.*?)\3?\s*\)~i', $style, $matches);
                $images = current($matches['image']);
                if ($images{0} == '/') {
                    $images = 'http:' . $images;
                }

                $check = DB::connection('mongodb')
                    ->table('habbo_images')
                    ->where('src', '=', $images)
                    ->count();

                if($check == 0){

                    $search = array();
                    $search[] = $images;
                    $search[] = (string)$conatiner->find('.title', 0)->innertext;

                    $image = array(
                        'src' => $images,
                        'search' => implode(' - ', $search),
                        "dt" => new MongoDate(strtotime(date("Y-m-d H:i:s")))
                    );

                    echo $images."\n";

                    DB::connection('mongodb')
                        ->table('habbo_images')
                        ->insert($image);

                }

                $linkContainer = $conatiner->find('.promo-link-container', 0);

                if($linkContainer){
                    $link = $linkContainer->find('a', 0);
                    if($link){
                        $newsUrl = $link->href;
                        if(strpos($newsUrl, 'articles') !== false) {
                            $newsUrl = str_replace('https', 'http', $newsUrl);

                            try {
                                $newsBody = new \Htmldom((string)$newsUrl);
                                $images = $newsBody->find('img');

                                foreach ($images as $img) {
                                    $src = $img->src;
                                    if ((strpos($src, 'habbo') !== false || strpos($src, 'aws') !== false || strpos($src, 'cdn') !== false)
                                        && strpos($src, 'adtech') === false
                                    ) {

                                        if ($src{0} == '/') {
                                            $src = 'http:' . $src;
                                        }

                                        $check = DB::connection('mongodb')
                                            ->table('habbo_images')
                                            ->where('src', '=', $src)
                                            ->count();

                                        if ($check == 0) {
                                            echo $src . "\n";

                                            $search = array();
                                            $search[] = $src;
                                            $search[] = trim((string)$newsBody->find('h2.title', 0)->innertext);
                                            $search[] = trim((string)$newsBody->find('#article-wrapper h2', 0)->innertext);

                                            $image = array(
                                                'src' => $src,
                                                'search' => implode(' - ', $search),
                                                "dt" => new MongoDate(strtotime(date("Y-m-d H:i:s")))
                                            );

                                            DB::connection('mongodb')
                                                ->table('habbo_images')
                                                ->insert($image);
                                        }
                                    }
                                }
                            }catch (Exception $e){
                                echo $e->getMessage()."\n";
                            }
                        }
                    }
                }

                /* OPEN NEWs */


            }

        }
    }

    private function rip()
    {

        $context = $this->getCookie();

        $tld = 'com';
        $selfis = $json = $this->request('https://www.habbo.com/stories-api/public/selfies', $context);//file_get_contents('https://www.habbo.com/stories-api/public/selfies', false, $context);
        $selfis = json_decode($selfis, true);
        if(count($selfis)) {
            $selfi = array_rand($selfis);
            $selfi = $selfis[$selfi];
            $habbo = $selfi['creator']['name'];

            $this->getHabbo($tld, $context, $habbo);
        }else{
            var_dump($json, $context);
        }


    }

    private function ripHabbo($habboName, $depth = 3, $lang = 'hhde')
    {

        $context = $this->getCookie();
        $this->getHabbo($lang, $context, $habboName, $depth, true);

    }

    private function analyHabbos()
    {

        echo 'SATRT';
        $count = $habbos = DB::table('habbo_users')
            ->whereNull('analytics_update')
            ->count();

        echo $count . ' Habbos werden analysiert!' . "\n\n";

        $partSize = 20;

        for ($i = 0; $i < ($count / $partSize); $i++) {

            $habbos = DB::table('habbo_users')
                ->whereNull('analytics_update')
                ->where(function ($query) {
                    $query->whereNotNull('badges')
                        ->orWhere(function ($query) {
                            $query->whereNotNull('friends');
                        })
                        ->orWhere(function ($query) {
                            $query->whereNotNull('badges');
                        })
                        ->orWhere(function ($query) {
                            $query->whereNotNull('rooms');
                        });
                })
                ->skip(($partSize * $i))
                ->take($partSize)
                ->get(array('id'));

            echo "#### Start Part (" . ($i + 1) . ") #### \n";

            foreach ($habbos as $id) {
                echo $id['id'] . "\n";
                $this->analyHabbo($id['id']);
                //$habbos[$id] = null;
            }


        }
    }

    private function jobRunning($name) {
        $ps = shell_exec("ps -aux | grep '".$name."' | grep -v 'grep' | grep -v '0:00'");
        if(strpos($ps, $name) !== false){
            echo "\n\nis Running!\n\n\n";
            var_dump($ps);
            return true;
        }else{
            return false;
        }
    }

    public function sign()
    {
        $habbos = DB::table('habbo_analytics')->get(array('habbo_id'));

        $i = 0;
        foreach ($habbos as $id) {
            $i++;
            DB::table('habbo_users')
                ->where('id', '=', new MongoId($id['habbo_id']))
                ->whereNull('analytics_update')
                ->update(array(
                    'analytics_update' => new MongoDate(strtotime("+2 hour"))
                ));

            echo $i . ' - ' . $id['habbo_id'] . ' CHECK!' . "\n";

        }

    }

    private function analyHabbo($habboId)
    {

        $habbo = DB::table('habbo_users')
            ->where('id', '=', $habboId)
            ->first(array('rooms', 'friends', 'badges', 'groups'));


        if ($habbo != null) {
            $analytics = array();

            $rooms = 0;
            $friends = 0;
            $badges = 0;
            $groups = 0;

            if (isset($habbo['rooms'])) {
                $rooms = count($habbo['rooms']);
            }
            $analytics['count_rooms'] = $rooms;

            if (isset($habbo['friends'])) {
                $friends = count($habbo['friends']);
            }
            $analytics['count_friends'] = $friends;


            if (isset($habbo['badges'])) {
                $badges = count($habbo['badges']);
            }
            $analytics['count_badges'] = $badges;

            if (isset($habbo['groups'])) {
                $groups = count($habbo['groups']);
            }
            $analytics['count_groups'] = $groups;

            $check = DB::table('habbo_analytics')
                ->where('habbo_id', '=', $habboId)
                ->first();

            DB::table('habbo_users')
                ->where('id', '=', $habboId)
                ->update(array(
                        'analytics_update' => new MongoDate(strtotime("now"))
                    )
                );

            if ($check != null) {

                DB::table('habbo_analytics')
                    ->where('habbo_id', '=', $habboId)
                    ->update($analytics);

            } else {

                $analytics['habbo_id'] = $habboId;
                DB::table('habbo_analytics')
                    ->insert($analytics);

            }
        }

    }


    private function getCookie($url = 'http://www.habbo.com/api/public/users?name=DE-cundero')
    {
        $getCookie = $this->request($url, '');//file_get_contents($url);
        if (strpos($getCookie, 'setCookie') !== false) {
            $cookie = explode('setCookie(', $getCookie);
            foreach ($cookie as $key => $find) {
                if ($find{0} == "'" && strpos($find, 'document.referrer') === false) {
                    $cookie = $cookie[$key];
                }
            }

            $cookie = explode(');', $cookie);
            $cookie = current($cookie);
            $cookie = explode("',", $cookie);

            foreach ($cookie as &$value) {
                $value = trim($value);
                if ($value{0} == "'" || $value{0} == " ") {
                    $value = substr($value, 1);
                }
            }

            list($name, $value, $exp) = $cookie;


            return trim($name)."=".trim($value);

        } else {
            return $this->getCookie('https://www.habbo.com/stories-api/public/selfies');
        }

    }

    private function getHabbo($tld, $urlHeader, $habboName, $depth = 1, $force = false)
    {

        if($depth > 0) {

            $habboName = urlencode($habboName);

            echo "start in ".$tld." for habbo ".$habboName."\n";
            var_dump( 'http://www.habbo.com/api/public/users?name=' . ($habboName) . '&site=' . $tld."\n");
            if ($depth % 2 && $depth < 4) {
                echo "## NEW HEADER ##\n";
                $urlHeader = $this->getCookie();
            }


            $habbo = $this->request('http://www.habbo.com/api/public/users?name=' . ($habboName) . '&site=' . $tld, $urlHeader);//file_get_contents(//'http://www.habbo.com/api/public/users?name=' . rawurldecode($habboName) . '&site=' . $tld, false, $urlHeader);

            echo'LOAD;'."\n";

            if ($habbo != '') {
                //sleep(rand(1, 2));
                //$parsed = preg_replace('/\]\](\r|\n|\r\n|\n\r)\[\[/', '],[', $habbo);
                $json = json_decode($habbo, true);
                $habboId = $json['uniqueId']; //profileVisible

                if ($json['profileVisible'] == false) {
                    return false;
                }

                $this->ripped[] = $habboId;

                $habboData = Habbo::firstOrCreate(
                    array(
                        'habbo_id' => $habboId,
                        'tld' => $tld
                    )
                );

                if($habboData->last_rip && !empty($habboData->last_rip) && $habboData->last_rip != '0000-00-00 00:00:00') {
                    $checkDate = Date::parse($habboData->last_rip);

                    if ($checkDate > Date::now()->subHour()) {
                        echo 'BREAK;';
                        return true;
                    }
                }


                echo "Rip " . $habboData->name . " (" . $habboId . ") | $tld" . "\n";
                //sleep(rand(1, 2));


                $profile = $this->request('https://www.habbo.com/api/public/users/' . $habboId . '/profile', $urlHeader);//file_get_contents('https://www.habbo.com/api/public/users/' . $habboId . '/profile', false, $urlHeader);
                echo " START " . date('H:i:s') . "\n";
                $json = json_decode($profile);

                if ($habboData->updated_at->addDay() < Date::now()
                    || $habboData->name == ''
                    || empty($habboData->name)
                    || !isset($habboData->badges)
                    || empty($habboData->badges)
                    || $force == true
                ) {

                    $habboData->name = $json->user->name;

                    $habboData->tld = $tld;
                    $habboData->habbo_created = new MongoDate(strtotime($json->user->memberSince));
                    $habboData->motto = $json->user->motto;

                    $habboData->last_rip = new MongoDate();

                    $habboBadges = array();
                    if (isset($json->badges)) {
                        echo "   BADGES " . date('H:i:s') . "\n";
                        $badges = $json->badges;
                        $countBadges = count($badges);

                        foreach ($badges as $badge) {
                            $habboBadges[] = $this->setBadge($badge, $tld);
                        }
                    }

                    foreach ($habboBadges as $badge) {
                        $groupData = HabboBadges::firstOrCreate(array(
                            'badge_id' => $badge,
                            'habbo_id' => $habboData->id
                        ));
                    }

                    $habboRooms = array();
                    if (isset($json->rooms)) {
                        echo "   Rooms " . date('H:i:s') . "\n";
                        $rooms = $json->rooms;
                        foreach ($rooms as $room) {
                            $habboRooms[] = $this->getRoom($tld, $room);

                        }
                    }

                    foreach ($habboRooms as $room) {
                        $groupData = HabboRooms::firstOrCreate(array(
                            'room_id' => $room,
                            'habbo_id' => $habboData->id
                        ));
                    }

                    $habboGroups = array();
                    if (isset($json->groups)) {
                        echo "   Groups " . date('H:i:s') . "\n";
                        $groups = $json->groups;
                        foreach ($groups as $group) {
                            $habboGroups[] = array(
                                "id" => $this->getGroup($tld, $group, $habboData->id),
                                "admin" => (isset($group->isAdmin) && $group->isAdmin) ? true : false
                            );
                        }
                    }


                    foreach ($habboGroups as $group) {
                        if ($group['id'] != false) {
                            $groupData = HabboGroups::firstOrCreate(array(
                                'group_id' => $group['id'],
                                'habbo_id' => $habboData->id
                            ));

                            $groupData->is_admin = $group['admin'];
                            $groupData->save();
                        }
                    }

                    $habboFriends = array();
                    $afterFriends = array();
                    if (isset($json->friends)) {
                        echo "   Friends " . date('H:i:s') . "\n";
                        $friends = $json->friends;
                        foreach ($friends as $friend) {
                            $f = Habbo::where('habbo_id', '=', $friend->uniqueId)->first();
                            if (isset($f->id) && $f->id) {
                                $habboFriends[] = $f->id;
                            } else {
                                $afterFriends[] = $friend->uniqueId;
                            }
                        }
                    }

                    foreach ($habboFriends as $friends) {
                        $groupData = HabboFriends::firstOrCreate(array(
                            'friend_id' => $friends,
                            'habbo_id' => $habboData->id
                        ));
                    }


                    echo " END " . date('H:i:s') . "\n";

                    $habboData->last_rip = date('Y-m-d H:i:s');
                    $habboData->save();
                } else {
                    $habboData->updated_at->subDay();
                    echo " OK! " . $habboData->updated_at->format('d.m.y H:i:s') . "\n";
                }

                //RIP FIRENDS
                if (isset($json->friends)) {
                    $friends = $json->friends;
                    foreach ($friends as $friend) {
                        if (!in_array($friend->uniqueId, $this->ripped)) {
                            $this->getHabbo($tld, $urlHeader, $friend->name, ($depth - 1));
                        }
                    }

                    foreach ($afterFriends as $friend) {
                        $f = Habbo::where('habbo_id', '=', $friend)->first();
                        if (isset($f->id) && $f->id) {
                            $habboFriends[] = $f->id;
                        }
                    }
                }


            } else if ($depth < 4) {
                echo "ERROR: " . $habboName;
            }
        }
    }

    private function getGroup($tld, $group, $habboId = null)
    {
        if(isset($group->roomId)) {
            $room = Room::where('room_id', '=', $group->roomId)->first();
            if (isset($room->id) && $room->id) {
                $groupData = Group::firstOrCreate(array(
                    'group_id' => $group->id,
                    'tld' => $tld,
                    'room_id' => $this->getRoom($tld, (int)$room->id, $habboId)
                ));

                $groupData->name = $group->name;
                $groupData->description = $group->description;
                $groupData->primaryColour = $group->primaryColour;


                if (isset($group->secondaryColour))
                    $groupData->secondaryColour = $group->secondaryColour;

                $groupData->badgeCode = $group->badgeCode;
                //$groupData->room_id = $group->roomId;
                $groupData->type = $group->type;

                $id = $groupData->id;
                $groupData->save();
                return $id;
            } else {
                return false;
            }
        }else{
            return false;
        }

    }

    private function getRoom($tld, $room, $habboId = null)
    {
        $roomData = null;
        if(is_object($room) && $habboId == null) {
            $habboId = Habbo::where('habbo_id', '=', $room->ownerUniqueId)->first();
            $roomData = Room::firstOrCreate(array(
                'room_id' => $room->id,
                'tld' => $tld,
                'habbo_own' => $habboId->id,
            ));


            $roomData->name = $room->name;
            $roomData->description = $room->description;
            $roomData->save();

        }else{
            if(!is_numeric($habboId)){
                echo'FALSE ROOM HABBO ID'; die();
            }
            $roomData = Room::firstOrCreate(array(
                'room_id' => $room,
                'tld' => $tld,
                'habbo_own' => $habboId
            ));
        }
        $roomId = $roomData->id;
        return $roomId;
    }

    private function setBadge($badge, $tld)
    {
        $check = DB::table('habbo_badges')
            ->where('code', '=', $badge->code)
            ->first();

        $badgeId = null;

        $tld = str_replace('hhus', 'com', $tld);
        $tld = str_replace('hh', '', $tld);

        if ($check === null) {
            $badgeId = DB::table('habbo_badges')->insertGetId(
                array(
                    'code' => $badge->code,
                )
            );

            DB::table('habbo_badges_name')->insert(
                array(
                    'badge_id' => $badgeId,
                    'lang' => str_replace('hh', '', $tld),
                    'name' => $badge->name
                )
            );

            DB::table('habbo_badges_description')->insert(
                array(
                    'badge_id' => $badgeId,
                    'lang' => str_replace('hh', '', $tld),
                    'description' => $badge->description
                )
            );

        } else {
            $description = DB::table('habbo_badges_description')
                ->where('badge_id', '=', $check->id)
                ->where('lang', '=', $tld)
                ->first();

            $badgeId = $check->id;

            if ($description == null) {
                DB::table('habbo_badges_description')->insert(
                    array(
                        'badge_id' => $badgeId,
                        'lang' =>  str_replace('hh', '', $tld),
                        'description' => $badge->description
                    )
                );
            }

            $description = DB::table('habbo_badges_name')
                ->where('badge_id', '=', $check->id)
                ->where('lang', '=', str_replace('hh', '', $tld))
                ->first();

            $badgeId = $check->id;

            if ($description == null) {
                DB::table('habbo_badges_name')->insert(
                    array(
                        'badge_id' => $badgeId,
                        'lang' => str_replace('hh', '', $tld),
                        'name' => $badge->name
                    )
                );
            }

        }

        return $badgeId;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('action', InputArgument::REQUIRED, 'Download or Rip'),
            array('habbo', InputArgument::OPTIONAL, ''),
            array('tld', InputArgument::OPTIONAL, ''),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

    public function request ($url, $cookies) {

        $header = array();
        $header[] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5';
        $header[] = 'Cache-Control: max-age=0';
        $header[] = 'Connection: keep-alive';
        $header[] = 'Keep-Alive: 300';
        $header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';
        $header[] = 'Accept-Language: en-us,en;q=0.5';
        $header[] = 'Pragma: ';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729)');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_COOKIE, $cookies);
        return curl_exec($ch);
    }
}
<?php

ini_set('memory_limit', '512M');

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RipperFurni extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ripper:furni';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $action = $this->argument('action');

        switch ($action) {
            case 'download':
                $this->download();
                break;
            case 'upload':
                $this->uploadAws();
                break;
            case 'rip':

                if(!$this->jobRunning($this->name." rip")) {
                    $furnis = DB::connection('mongodb')->table('furnis')->where('furni.image', '=', null)->get();

                    foreach ($furnis as $furni) {
                        $this->ripper((string)$furni['_id']);
                    }

                    $furnis = DB::connection('mongodb')
                        ->table('furnis')
                        ->where('furni.icon_image', '=', null)
                        ->orderBy('dt', 'desc')
                        ->get();

                    foreach ($furnis as $furni) {
                        $this->iconRip($furni['_id']);
                    }
                }

                break;
            case 'ripPlus':

                $furni = DB::connection('mongodb')
                    ->table('furnis')
                    //->where('furni.name', '=', 'room_info15_gate name')//'Group Flag')
                    //->where('furni.name', '=', 'Group Flag')
                    //->where('furni.name', '=', 'Polar Sofa')
                    //->where('furni.image', '=', null)
                    //->where('furni.name', '=', 'Darkstar Dragons')
                    //->where('animated.clear', '=', null)
                    //->orWhere('animated.gifs', '=', null)
                    ->where('logic', '=', null)
                    ->where('dt', '>', new MongoDate(strtotime("2015-06-27 00:00:00")))
                    ->where('work', '=', null)
                    ->orderBy('dt', 'desc')
                    ->first();

                DB::connection('mongodb')
                    ->table('furnis')
                    ->where('_id', '=', (string)$furni['_id'])
                    ->update(array(
                        'work' => 1
                    ));

                ini_set('memory_limit', '512M');

                $this->ripperPlus((string)$furni['_id']);

                $furnis = DB::connection('mongodb')
                    ->table('furnis')
                    ->where('furni.icon_image', '=', null)
                    ->orderBy('dt', 'desc')
                    ->get();

                foreach ($furnis as $furni) {
                    $this->iconRip($furni['_id']);
                }


                break;
        }
    }

    private function uploadAws () {
        $url = 'http://furnis.habboindex.com/';

        $furnis = DB::connection('mongodb')
            ->table('furnis')
            //->where('furni.name', '=', 'room_info15_gate name')//'Group Flag')
            //->where('furni.name', '=', 'Group Flag')
            //->where('furni.name', '=', 'Polar Sofa')
            //->where('furni.image', '=', null)
            //->where('furni.name', '=', 'Darkstar Dragons')
            //->where('animated.clear', '=', null)
            //->orWhere('animated.gifs', '=', null)
            ->where('upload', '=', null)
            ->where('dt', '>', new MongoDate(strtotime("2015-05-01 00:00:00")))
            ->whereNotNull('logic')
            ->orderBy('dt', 'desc')
            ->get();

        $s3 = AWS::get('s3');

        foreach($furnis as $furni){

            $logic = $furni['logic'];
            foreach($logic as $lId => $block){
                foreach($block as $bId => $image){
                    if(is_string($image) && !empty($image)) {
                        $fileName = basename($image);
                        $info = new SplFileInfo($image);
                        $ext = $info->getExtension();

                        $newName = md5($fileName).time().'.'.$ext;

                        $dir = $furni['swf']['name'];

                        try {
                            $new = $s3->putObject(array(
                                'Bucket' => 'furnis.habboindex.com',
                                'Key' => $dir . '/' . $newName,
                                'SourceFile' => $image,
                                'ACL'        => 'public-read',
                            ));

                            $newUrl = $dir . '/' . $newName;

                            $logic[$lId][$bId] = $url.$newUrl;

                        } catch (\Aws\S3\Exception\S3Exception $e) {
                            // The AWS error code (e.g., )
                            echo $e->getAwsErrorCode() . "\n";
                            // The bucket couldn't be created
                            echo $e->getMessage() . "\n";
                            die;
                        }
                    }
                }
            }
            echo $furni['_id']."\n";
            DB::connection('mongodb')
                ->table('furnis')
                ->where('_id', '=', new MongoId($furni['_id']))
                ->update(array(
                    'logic' => $logic,
                    'upload' => 'aws'
                ));


        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('action', InputArgument::REQUIRED, 'Download or Rip'),
        );
    }

    private function jobRunning($name) {
        $ps = shell_exec("ps -aux | grep '".$name."' | grep -v 'grep' | grep -v '0:00'");
        if(strpos($ps, $name) !== false){
            echo "\n\nis Running!\n\n\n";
            var_dump($ps);
            return true;
        }else{
            return false;
        }
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

    private function ripperPlus($furniId)
    {

//        $furni = DB::connection('mongodb')->table('furnis')->where('_id', '=', '5591279f27b81b0b038b4575')->first();// die();
        $furni = DB::connection('mongodb')->table('furnis')->where('_id', '=', $furniId)->first();
        $path = storage_path() . '/furniture_images/';

        if ($furni !== null) {

            if(!file_exists($furni['swf']['path'])){
                return false;
            }
            //swfextract $_GET['swf']
            $path .= $furni['revision'] . '_' . $furni['swf']['name'] . '/';
            exec('mkdir ' . $path);
            exec('chmod 777 ' . $path);
            exec('cd ' . $path . ' && swfextract ' . $furni['swf']['path'] . ' --outputformat "file_%04d.%s"  -a 1-', $get);
            exec('swfdump ' . $furni['swf']['path'], $get);

            //SITZE: 65 or 32

            $find = false;
            foreach ($get as $line) {
                if (strpos($line, "SYMBOLCLASS") !== false) {
                    $find = !$find;
                } else if (strpos($line, "exports") === false && $find)
                    $find = !$find;

                if ($find) {
                    $name = $line;
                    $name = explode(" ", trim($name));
                    $newName = str_replace('"', '', $name[3]);
                    $name = $name[1];
                    $get = '';
                    if ($name) {
                        exec("find " . $path . " -name 'file_" . $name . ".*' -exec bash -c 'mv $0 " . $path . $newName . "' {} \;", $get);
                        //echo $line.'<br>';
                    }
                }
            }

            $assets = glob($path . "/*_assets");
            $assets = file_get_contents($assets[0]);
            $assets = new SimpleXMLElement($assets);

            // IMAGE ORDER
            /*

            convert shelves_norja.png \
            \( mask.jpg -alpha set -channel A -evaluate set 60% \) \
            -compose screen -composite \
            destination.jpg

            convert shelves_norja.png -alpha on -channel a -evaluate set 25% result.png

            */

            $width = 0;
            $height = 0;

            $startI = 2;
            $shadowId = 0;

            $images = array();
            foreach ($assets as $asset) {
                $newName = '';
                $name = (string)$asset->attributes()->name[0];

                if (strpos($name, '64') !== false) {


                    preg_match('/_64_([a-z]*)_([0-9]*)_([0-9]*)/', $name, $filter);
                    if(count($filter)) {
                        list($match, $part, $direction, $frame) = $filter;
                    }else{
                        preg_match('/_64_([a-z]*)_([0-9]*)/', $name, $filter);
                        $frame = 0;
                        if(count($filter)) {
                            list($match, $part, $direction) = $filter;
                        }else{
                            continue;
                        }
                    }

                    exec("find " . $path . " -name '*" . $name . "'", $newName);

                    if (!count($newName)) {
                        if (!empty($asset->attributes()->source)) {
                            //exec("find " . $path . " -name '*" . $asset->attributes()->source . "'", $sourceName);
                            $sourceName = shell_exec("find " . $path . " -name '*" . $asset->attributes()->source . "'");
                            if (empty($sourceName)) {
                                $newName = "";
                            }else{
                                $newName = $sourceName;
                            }
                        }
                    }



                    if(empty($newName)){
                        continue;
                    }

                    if(is_array($newName)){
                        $newName = current($newName);
                    }

                    $sourceName = $asset->attributes()->source;
                    if (!isset($asset->attributes()->source) || empty($asset->attributes()->source)) {
                        $size = getimagesize($newName);
                    } else {
                        exec("find " . $path . " -name '*" . $asset->attributes()->source . "'", $sourceName);
                        if (count($sourceName) == 1) {
                            if(empty($sourceName)){
                                continue;
                            }else{
                                if(is_array($sourceName)){
                                    $sourceName = current($sourceName);
                                }
                            }
                            $size = getimagesize($sourceName);
                        }
                    }


                    $width += $size[0];
                    $height += $size[1];


                    $data = array(
                        'path' => $newName,
                        'size' => array(
                            'width' => $size[0],
                            'height' => $size[1]
                        ),
                        'position' => array(
                            'x' => $asset->attributes()->x,
                            'y' => $asset->attributes()->y
                        ),
                        'source' => $sourceName,
                        'flipH' => $asset->attributes()->flipH,
                    );

                    $images[$direction][$frame][$part] = $data;

                }


            }

            $assets = null;


            foreach ($images as $direction => $frames) {
                foreach ($frames as $frame => $parts) {
                    if (!isset($parts['a'])) {
                        if (isset($images[$direction][$frame - 1]['a'])) {
                            $images[$direction][$frame]['a'] = $images[$direction][$frame - 1]['a'];
                        }
                    }
                }
            }

            // IMAGE SIZE

            if ($width > 2500) {
                $width = 2500;
            }else if($width < 1500){
                $width = 1500;
            }
            if ($height > 2500) {
                $height = 2500;
            }else if($height < 1500){
                $height = 1500;
            }



            $rippedImages = array();
            $smallestDirection = 99999999;

            $movePosX = 0;
            $movePosY = 0;

            foreach ($images as $direction => $frames) {
                if ($width != 0 && $height != 0) {

                    foreach ($frames as $frame => $parts) {
                        $flip = false;
                        $image = imagecreatetruecolor($width, $height);
                        if ($image) {
                            imagealphablending($image, false);
                            $col = imagecolorallocatealpha($image, 123, 0, 0, 127);
                            imagefill($image, 0, 0, $col);
                            imagecolortransparent($image, $col);
                            imagealphablending($image, true);

                            $images = array();
                            $shadow = null;

                            if (true) {
                                foreach ($parts as $part => $imageData) {
                                    if ($part == 'sd') {
                                        continue;
                                    }

                                    if ($imageData['flipH'] == 1) {
                                        $flip = true;
                                    }

                                    $name = $imageData['path'];
                                    if (!empty($imageData['source'])) {
                                        $name = $imageData['source'];
                                    }

                                    if (($size = getimagesize($name)) && strpos($name, '64') !== false) {

                                        if (strpos($imageData['path'], '_sd_') === false) {

                                            $images[$name] = imageCreateFromPNG($name);
                                            imageSaveAlpha($images[$name], true);
                                            //filter_opacity($images[$name], 80);
                                            imageCopy(
                                                $image,
                                                $images[$name],
                                                ($width / 2) - (int)$imageData['position']['x'],
                                                ($height / 2) - (int)$imageData['position']['y'],     /* imagecopy() an die Stelle ( 0, 0) in $Wiese, */
                                                0,
                                                0,     /* der zu kopierende Bereich beginnt in $Sonne bei ( 0, 0) */
                                                $size[0],
                                                $size[1]
                                            );

                                            if($movePosX < (int)$imageData['position']['x']){
                                                $movePosX = (int)$imageData['position']['x'];
                                            }

                                            if($movePosY < (int)$imageData['position']['y']){
                                                $movePosY = (int)$imageData['position']['y'];
                                            }


                                        } else {
                                        }
                                    }

                                }

                                $imageName = str_replace(".", "_", $furni['swf']['name']);
                                $imageName .= '_d' . $direction . '_f' . $frame;
                                $img = $path . $imageName . '.png';
                                imagepng($image, $img);
                                if ($shadow !== null) {
                                    $size = getimagesize($img);
                                    exec('convert  -size ' . $size[0] . 'x' . $size[1] . ' xc:none ' . $shadow['img'] . ' -geometry +' . $shadow['x'] . '+' . $shadow['y'] . ' -composite ' . $img . ' -geometry +0+0 -composite ' . $img);
                                }

                                exec('convert ' . $img . ' -trim ' . $img.'.trim');
                                if ($flip) {
                                    exec('convert ' . $img . ' -flop ' . $img);
                                }

                                $image = null;
                            } else {
                                $imageName = str_replace(".", "_", $furni['swf']['name']);
                                $imageName .= '_d' . $direction . '_f' . $frame;
                                $img = $path . $imageName . '.png';
                            }


                            if ($smallestDirection > $direction) {
                                $smallestDirection = $direction;
                            }
                            $rippedImages[$direction][$frame] = $img;
                            $rippedTrimImages[$direction][$frame] = $img.'.trim';
                            echo $img . "\n";


                        }
                    }
                }
            }

            $maxTrimHeight = 0;
            $maxTrimWidth = 0;
            foreach($rippedTrimImages as $directions) {
                foreach($directions as $img) {
                    $size = getimagesize($img);
                    if (is_array($size) && count($size) > 1) {

                        if ($maxTrimHeight < $size[1]) {
                            $maxTrimHeight = $size[1];
                        }

                        if ($maxTrimWidth < $size[0]) {
                            $maxTrimWidth = $size[0];
                        }
                    }
                }
            }

            DB::connection('mongodb')->table('furnis')->where('_id', '=', $furniId)->update(array(
                "animated.clear" => $rippedTrimImages
            ));


            $visualization = glob($path . "/*_visualization");
            $visualization = file_get_contents($visualization[0]);
            $visualization = new SimpleXMLElement($visualization);

            $animationsArray = array();
            $directionArray = array();

            foreach($visualization as $key => $groups){
                echo __LINE__."\n";
                if($key == 'graphics') {
                    foreach($groups as $childKey => $visual) {
                        if($visual->attributes()->size == '64') {
                            foreach($visual as $configKey => $configData) {
                                echo __LINE__."\n";
                                if($configKey == 'directions') {
                                    foreach($configData as $direction){
                                        $imageId = $direction->attributes()->id;
                                        echo __LINE__."\n";
                                        if(isset($rippedImages[(int)$imageId])) {
                                            $directionArray[(int)$imageId] = current($rippedTrimImages[(int)$imageId]);
                                        }
                                    }
                                    echo __LINE__."\n";
                                }else if($configKey == 'animations') {
                                    foreach($configData as $animation){
                                        $animationId = $animation->attributes()->id;

                                        foreach($animation as $layer){
                                            echo __LINE__."\n";
                                            $layerId = $layer->attributes()->id;
                                            if(!empty($layer)){
                                                if(isset($layer->frameSequence)){
                                                    $frames = $layer->frameSequence;
                                                    $images = array();

//                                                    if(is_object($frames->frame)){
//                                                        $frames = array($frames->frame);
//                                                    }else{
//                                                        $frames = $frames->frame;
//                                                    }

                                                    foreach($layer as $endKey => $frameSequence){
                                                        echo __LINE__."\n";
                                                        foreach($frameSequence as $frame) {
                                                            $frameId = $frame->attributes()->id;

                                                            if ($frameId && isset($rippedImages[$smallestDirection][(int)$frameId])) {
                                                                $images[] = $rippedImages[$smallestDirection][(int)$frameId];
                                                            }
                                                        }
                                                    }

                                                    if(count($images) > 1) {
                                                        $EndSize = 2000;
                                                        //$pos = " -gravity northeast";
                                                        $imageString = '';
                                                        $maxHeight = 0;
                                                        $maxWidth = 0;

                                                        foreach ($images as $img) {
                                                            $size = getimagesize($img);

                                                            if ($maxHeight < $size[1]) {
                                                                $maxHeight = $size[1];
                                                            }

                                                            if ($maxWidth < $size[0]) {
                                                                $maxWidth = $size[0];
                                                            }

                                                        }
                                                        echo __LINE__."\n";

                                                        foreach ($images as $img) {
                                                            echo __LINE__."\n";
                                                            $size = getimagesize($img);
                                                            $imageString .= ' -page +0+' . ($maxHeight - $size[1]) . ' ' . $img;
                                                        }
                                                        echo __LINE__."\n";
                                                        //$imageString = implode(' '.$pos.' ', $images);

                                                        $aniName = str_replace(".", "_", $furni['swf']['name']);
                                                        $aniName .= '_ani' . $animationId . '_lay' . $layerId;
                                                        $aniNameCrop = $path . $aniName . '_crop2.gif';
                                                        $aniName = $path . $aniName . '.gif';

                                                        echo __LINE__."\n";

                                                        exec('rm ' . $aniName . ' -f');
                                                        $exec = 'convert -delay 15 -dispose Background -size ' . $maxWidth . 'x' . $maxHeight . ' ' . $imageString . '  -loop 0 ' . $aniName;
                                                        exec($exec);

                                                        exec('gifsicle -b --crop-transparency '.$aniName.' --colors 256');

                                                        //exec('convert ' . $aniName . ' -trim ' . $aniName);
                                                        //exec('convert '.$aniName.' -coalesce -repage 0x0 -crop '.$maxTrimWidth.'x'.$maxTrimHeight.'+'.$posX.'+'.$posY.' +repage '.$aniName);
                                                        //exec('convert '.$aniName.'  -trim +repage -gravity South '.$aniName);
                                                        //var_dump('convert '.$aniName.'  -trim +repage -gravity South '.$aniName);

                                                        //$crop = 'convert ' . $aniName . ' -shave 0x' . ($EndSize - $maxHeight) . ' -gravity SouthWest +repage  ' . $aniNameCrop . ' ';
                                                        //exec($crop);// /usr/bin/convert -crop [Breite Ausschnitt]x[Höhe Ausschnitt]+[Beginn x]+[Beginn y] ausgabedatei eingabedatei”);
                                                        //var_dump($crop);
                                                        echo __LINE__."\n";

                                                        $animationsArray[] = $aniName;
                                                        echo $aniName . "\n";

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            DB::connection('mongodb')->table('furnis')->where('_id', '=', $furniId)->update(array(
                "logic" => array(
                    'directions' => $directionArray,
                    'click' => $animationsArray,
                ),
                'work' => null
            ));

            $visualization = null;
            $directionArray = null;
            $animationsArray = null;

            Cache::flush();
        }
    }


    private
    function ripper($furniId)
    {

        $furni = DB::connection('mongodb')->table('furnis')->where('_id', '=', $furniId)->first();
        $path = storage_path() . '/furniture_images/';

        if ($furni !== null) {
            //swfextract $_GET['swf']
            $path .= $furni['revision'] . '_' . $furni['swf']['name'] . '/';
            exec('mkdir ' . $path);
            exec('chmod 777 ' . $path);
            exec('cd ' . $path . ' && swfextract ' . $furni['swf']['path'] . ' --outputformat "file_%04d.%s"  -a 1-', $get);
            exec('swfdump ' . $furni['swf']['path'], $get);

            //SITZE: 65 or 32

            $find = false;
            foreach ($get as $line) {
                if (strpos($line, "SYMBOLCLASS") !== false) {
                    $find = !$find;
                } else if (strpos($line, "exports") === false && $find)
                    $find = !$find;

                if ($find) {
                    $name = $line;
                    $name = explode(" ", trim($name));
                    $newName = str_replace('"', '', $name[3]);
                    $name = $name[1];
                    $get = '';
                    if ($name) {
                        exec("find " . $path . " -name 'file_" . $name . ".*' -exec bash -c 'mv $0 " . $path . $newName . "' {} \;", $get);
                        //echo $line.'<br>';
                    }
                }
            }

            //echo'<br>';
            $assets = glob($path . "/*_assets");
            $assets = file_get_contents($assets[0]);
            $assets = new SimpleXMLElement($assets);

            // IMAGE ORDER
            /*

            convert shelves_norja.png \
            \( mask.jpg -alpha set -channel A -evaluate set 60% \) \
            -compose screen -composite \
            destination.jpg

            convert shelves_norja.png -alpha on -channel a -evaluate set 25% result.png

            */

            $width = 0;
            $height = 0;

            $startI = 2;
            $shadowId = 0;

            $assentArray = array();
            foreach ($assets as $asset) {
                $newName = '';
                $name = (string)$asset->attributes()->name[0];

                if (strpos($name, '64_a_2') !== false
                    || strpos($name, '64_b_2') !== false
                    || strpos($name, '64_a_1') !== false
                    || strpos($name, '64_b_1') !== false
                ) {
                    exec("find " . $path . " -name '*" . $name . "'", $newName);

                    if (isset($newName[0])) {

                        $putId = $startI;
                        if (strpos($name, "_sd") !== false) {
                            $putId = $shadowId;
                            $shadowId++;
                        } else {
                            $startI++;
                        }

                        $name = $newName[0];
                        $assentArray[$putId] = array();
                        $assentArray[$putId]['x'] = $asset->attributes()->x;
                        $assentArray[$putId]['y'] = $asset->attributes()->y;
                        $assentArray[$putId]['source'] = $asset->attributes()->source;
                        $assentArray[$putId]['name'] = $name;
                        $assentArray[$putId]['flipH'] = $asset->attributes()->flipH;
                        if (($name && empty($assentArray[$putId]['flipH'])) && strpos($name, '64') !== false) {
                            $size = getimagesize($name);
                            $width += $size[0];
                            $height += $size[1];
                        } else {
                            //echo $name;
                        }
                    }
                }
            }


            // IMAGE SIZE


            if ($width < 10000) {
                $width *= 3;
            }
            if ($height < 10000) {
                $height *= 3;
            }
            if ($width != 0 && $height != 0) {
                $image = imagecreatetruecolor($width, $height);
                imagealphablending($image, false);
                $col = imagecolorallocatealpha($image, 123, 0, 0, 127);
                imagefill($image, 0, 0, $col);
                imagecolortransparent($image, $col);
                imagealphablending($image, true);

                $images = array();

                ksort($assentArray);
                $shadow = null;

                foreach ($assentArray as $key => $assent) {
                    $name = $assent['name'];
                    if (($size = getimagesize($name)) && strpos($name, '64') !== false) {

                        if (strpos($assent['name'], '_sd_') !== false) {
                            if ($shadow == null) {
                                $newName = $assent['name'] . '-a.png';
                                //convert test.png -fill '#CCCCCC00' -opaque none image.png
                                exec("convert " . $assent['name'] . " -fill '#01DF01' -opaque none " . $assent['name'], $output);
                                exec("convert " . $assent['name'] . " -matte -channel a -evaluate set 20% " . $newName, $output);
                                exec("convert " . $newName . " -transparent 'srgba(1,223,1,0.2)' " . $newName, $output);
                                $shadow = array(
                                    "img" => $newName,
                                    "x" => ($width / 2) - (int)$assent['x'],
                                    "y" => ($height / 2) - (int)$assent['y']
                                );
                            }
                            //convert shelves_norja.png -alpha on -channel a -evaluate set 25% result.png
                        } else {

                            $images[$name] = imageCreateFromPNG($name);
                            imageAlphaBlending($images[$name], true);
                            imageSaveAlpha($images[$name], true);
                            //filter_opacity($images[$name], 80);
                            imageCopy(
                                $image,
                                $images[$name],
                                ($width / 2) - (int)$assent['x'],
                                ($height / 2) - (int)$assent['y'],     /* imagecopy() an die Stelle ( 0, 0) in $Wiese, */
                                0,
                                0,     /* der zu kopierende Bereich beginnt in $Sonne bei ( 0, 0) */
                                $size[0],
                                $size[1]
                            );
                        }
                    }

                }

                DB::connection('mongodb')->table('furnis')->where('_id', '=', $furniId)->update(array(
                    "furni.image" => $path . str_replace(".", "_", $furni['swf']['name']) . '.png',
                    "dt" => new MongoDate(strtotime(date("Y-m-d H:i:s")))
                ));

                imagepng($image, $path . str_replace(".", "_", $furni['swf']['name']) . '.png');
                $img = $path . str_replace(".", "_", $furni['swf']['name']) . '.png';

                if ($shadow !== null) {
                    $size = getimagesize($img);
                    exec('convert  -size ' . $size[0] . 'x' . $size[1] . ' xc:none ' . $shadow['img'] . ' -geometry +' . $shadow['x'] . '+' . $shadow['y'] . ' -composite ' . $img . ' -geometry +0+0 -composite ' . $img);
                }

                exec('convert ' . $img . ' -trim ' . $img);
                exec("find " . $path . " ! -name '" . str_replace(".", "_", $furni['swf']['name']) . ".png' -type f | xargs rm -f");
                //exec('rm '.$furni['swf']['path'].' -f');

                echo $img . "\n";

            }
        }
    }


    private
    function iconRip($furniId)
    {

        $furni = DB::connection('mongodb')->table('furnis')->where('_id', '=', $furniId)->first();
        $path = storage_path() . '/furniture_images/icons/';

        if ($furni !== null) {

            if (!file_exists($furni['swf']['path'])) {
                return false;
            }

            //swfextract $_GET['swf']
            $path .= $furni['revision'] . '_' . $furni['swf']['name'] . '/';
            exec('mkdir ' . $path);
            exec('chmod 777 ' . $path);
            exec('cd ' . $path . ' && swfextract ' . $furni['swf']['path'] . ' --outputformat "file_%04d.%s"  -a 1-', $get);
            exec('swfdump ' . $furni['swf']['path'], $get);

            //SITZE: 65 or 32

            $find = false;
            foreach ($get as $line) {
                if (strpos($line, "SYMBOLCLASS") !== false) {
                    $find = !$find;
                } else if (strpos($line, "exports") === false && $find)
                    $find = !$find;

                if ($find) {
                    $name = $line;
                    $name = explode(" ", trim($name));
                    $newName = str_replace('"', '', $name[3]);
                    $name = $name[1];
                    $get = '';
                    if ($name) {
                        exec("find " . $path . " -name 'file_" . $name . ".*' -exec bash -c 'mv $0 " . $path . $newName . "' {} \;", $get);
                        //echo $line.'<br>';
                    }
                }
            }

            //echo'<br>';

            $assets = glob($path . "/*_assets");
            $assets = file_get_contents($assets[0]);
            $assets = new SimpleXMLElement($assets);

            $newFileName = '';
            $orgiFileName = '';
            foreach ($assets as $asset) {
                $name = (string)$asset->attributes()->name[0];
                if (strpos($name, 'icon') !== false) {
                    $iconFile = '';
                    exec("find " . $path . " -name '*" . $name . "'", $iconFile);
                    if ($iconFile) {
                        $fileName = current($iconFile);
                        $orgiFileName = $name;
                        $image = imageCreateFromPNG($fileName);
                        imageAlphaBlending($image, true);
                        imageSaveAlpha($image, true);

                        $newFileName = str_replace(".", "_", $fileName) . '.png';
                        imagepng($image, $newFileName);
                        break;
                    }
                }
            }

            DB::connection('mongodb')->table('furnis')->where('_id', '=', $furniId)->update(array(
                "furni.icon_image" => $newFileName
            ));

            exec("find " . $path . " ! -name '*.png' -type f | xargs rm -f");
            //exec('rm '.$furni['swf']['path'].' -f');

            echo $newFileName . "\n";

        }
    }


    private
    function download()
    {
        # Furni SWF ripping
        # Copyright (C) 2013 - 2014 Tha

        $path = storage_path() . '/furniture/';

        if (!is_dir($path)) {
            mkdir($path);
        }

        $urls = array(
            'http://habbo.com/gamedata/furnidata/0',
            'http://sandbox.habbo.com/gamedata/furnidata/0',
            'http://habbo.fi/gamedata/furnidata/0',
            'http://habbo.de/gamedata/furnidata/0'
        );

        foreach ($urls as $url) {
            $data = $this->request($url, '');
            //$data = simplexml_load_string('http://habbo.com/gamedata/furnidata_xml/0');
            $parsed = preg_replace('/\]\](\r|\n|\r\n|\n\r)\[\[/', '],[', $data);
            $json = json_decode($parsed, true);

            foreach ($json as $item) {
                $type = $item[0];
                $sprite_id = $item[1];
                $swf_name = $item[2];
                $x = explode('*', $swf_name);
                $swf_name = $x[0];
                $revision = $item[3];
                $name = $item[8];
                $desc = $item[9];

                $furni = DB::connection('mongodb')->table('furnis')->where('swf.name', '=', $swf_name)->first();

                if ($furni === null) {

                    if (!is_dir($path . $revision . '/')) {
                        mkdir($path . $revision . '/');
                    }

                    $swfPath = $path . $revision . "/" . $swf_name . ".swf";

                    $check = DB::connection('mongodb')->table('furnis')->where('swf.path', '=', $swfPath)->where('furni.image', '$not', null)->first();

                    if ($check === null) {
                        if (!@copy("http://images.habbo.com/dcr/hof_furni/$revision/$swf_name.swf", $swfPath)) {
                            echo "Cannot rip $revision/$swf_name.swf.\r\n";
                        } else {
                            DB::connection('mongodb')->table('furnis')->insert(
                                array(
                                    'type' => $type,
                                    'swf' => array(
                                        'name' => $swf_name,
                                        'sprite_id' => $sprite_id,
                                        'sprite_id' => $sprite_id,
                                        'path' => $swfPath
                                    ),
                                    'revision' => $revision,
                                    'furni' => array(
                                        'name' => $name,
                                        'description' => $desc
                                    ),
                                    "dt" => new MongoDate(strtotime(date("Y-m-d H:i:s")))
                                )
                            );
                            echo "Ripped $revision/$swf_name.swf.\r\n";
                        }
                    }
                }
            }
        }
    }

    public function request ($url, $cookies) {

        $header = array();
        $header[] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5';
        $header[] = 'Cache-Control: max-age=0';
        $header[] = 'Connection: keep-alive';
        $header[] = 'Keep-Alive: 300';
        $header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';
        $header[] = 'Accept-Language: en-us,en;q=0.5';
        $header[] = 'Pragma: ';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729)');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_COOKIE, $cookies);
        return curl_exec($ch);
    }
}


function filter_opacity(&$img, $opacity) //params: image resource id, opacity in percentage (eg. 80)
{
    if (!isset($opacity)) {
        return false;
    }
    $opacity /= 100;

    //get image width and height
    $w = imagesx($img);
    $h = imagesy($img);

    //turn alpha blending off
    imagealphablending($img, false);

    //find the most opaque pixel in the image (the one with the smallest alpha value)
    $minalpha = 127;
    for ($x = 0; $x < $w; $x++)
        for ($y = 0; $y < $h; $y++) {
            $alpha = (imagecolorat($img, $x, $y) >> 24) & 0xFF;
            if ($alpha < $minalpha) {
                $minalpha = $alpha;
            }
        }

    //loop through image pixels and modify alpha for each
    for ($x = 0; $x < $w; $x++) {
        for ($y = 0; $y < $h; $y++) {
            //get current alpha value (represents the TANSPARENCY!)
            $colorxy = imagecolorat($img, $x, $y);
            $alpha = ($colorxy >> 24) & 0xFF;
            //calculate new alpha
            if ($minalpha !== 127) {
                $alpha = 127 + 127 * $opacity * ($alpha - 127) / (127 - $minalpha);
            } else {
                $alpha += 127 * $opacity;
            }
            //get the color index with new alpha
            $alphacolorxy = imagecolorallocatealpha($img, ($colorxy >> 16) & 0xFF, ($colorxy >> 8) & 0xFF, $colorxy & 0xFF, $alpha);
            //set pixel with the new color + opacity
            if (!imagesetpixel($img, $x, $y, $alphacolorxy)) {
                return false;
            }
        }
    }
    return true;
}

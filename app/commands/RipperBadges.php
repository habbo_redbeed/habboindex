<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RipperBadges extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ripper:badge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $action = $this->argument('action');

        switch($action){
            case 'download':
                $this->download($this->argument('lang'));
                break;
            case 'downloadNew':
                $this->downloadNew($this->argument('lang'));
                break;
            case 'onlyImg':
                $this->downLoadImg();
                break;

            case 'random':
                $this->randomLoad();
                break;

            case 'exists':
                $this->badgeExists();
                break;
            case 'a_used':
                $this->analyseUsed();
                break;
        }
    }

    private function randomLoad () {
        $strings = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

        foreach($strings as $letterI){

            /// check


        }
    }

    private function findCombi ($string){
        $combis = [
            [
                'format' => "%'.03d",
                'count' => 50
            ],
            [
                'format' => "%'.02d",
                'count' => 20
            ],
            [
                'format' => "%'.03d",
                'count' => 3
            ]
        ];
    }

    private function badgeImageExists($string){
        $source = $this->request('http://images.habbo.com/c_images/album1584/'.$string.'.gif', '');
        if(strpos($source, '404') === false){
            return true;
        }else{
            return false;
        }
    }

    private function downLoadImg() {
        $list =  [
            'DE' => "%'.03d",
            'TR' => "%'.03d",
            'IT' => "%'.03d",
            'FR' => "%'.03d",
            'NL' => "%'.03d",
            'ES' => "%'.03d",
            'BR' => "%'.03d",
            'FI' => "%'.03d",
            'UK' => "%'.03d",
            'BR' => "%'.03d",
            'NT' => "%'.03d",

            'MX' => "%'.03d",
            'GAL' => "%'.02d",
            'APC' => "%'.02d",

            'APC' => "%'.02d",
            'APC' => "%'.02d",

		'ACH_Atcg_' => "%'.01d",

            'AC' => "%'.01d",
        ];

        foreach($list as $badgeName => $numberFormat){
            $check = DB::table('habbo_badges')
                ->where('code', 'LIKE', $badgeName.'%')
                ->lists('code');

            echo "START with ".$badgeName." ##\n";

            $fails = 0;
            $badgeCounter = 0;
            while($fails < 150 && $badgeCounter < 1000){
                $badge = $badgeName.sprintf($numberFormat, $badgeCounter);
                if(!in_array($badge, $check)){

                    $source = $this->request('http://images.habbo.com/c_images/album1584/'.$badge.'.gif', '');
                    if(strpos($source, '404') === false){

                        $fails = 0;
                        Badge::firstOrCreate(array('code' => $badge));

                    }else{
                        $fails ++;
                    }

                }

                $badgeCounter ++;
            }

            echo "END with ".$badgeName." at ".$badgeCounter." ##\n";
        }
    }

    private function analyseUsed() {
        $badges = DB::select('SELECT habbo_badges_name.`badge_id` as badge_id, habbo_badges_name.lang as lang, count(*) as used
            FROM `habbo_users_badges`
            LEFT JOIN habbo_badges_name ON (habbo_badges_name.`badge_id` = habbo_users_badges.`badge_id`)
             GROUP BY `badge_id`, habbo_badges_name.lang');

        foreach($badges as $badge){
            if(empty($badge->badge_id)){
                continue;
            }

            $used = BadgeUsed::firstOrCreate([
                'badge_id' => $badge->badge_id,
                'lang' => $badge->lang
            ]);

            $used->used = $badge->used;
            $used->save();

            echo "Save ".$badge->badge_id." - ".$badge->lang."\n";
        }
    }

    private function badgeExists() {
        $list = DB::table('habbo_badges')
            ->where(function ($query){
                $query->where('notfound', 0)
                    ->orWhere('notfound', '<=', \Carbon\Carbon::now()->subDay()->format('Y-m-d H:i:s'));
            })
            ->where('exists', 0)
            ->orderBy('updated_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->orderBy('code')
            ->get();

        echo 'START'."\n";
        foreach($list as $badge){
            $img = 'http://images.habbo.com/c_images/album1584/' . $badge->code . '.gif';
            $check = $this->get_headers_curl($img);
            if (!$check || strpos($check, '404') !== false) {
                echo "Badge ".$badge->code;
                Badge::find($badge->id)->update([
                    'notfound' => date('Y-m-d H:i:s')
                ]);
                echo " not exist \n";
            }else{
                Badge::find($badge->id)->update([
                    'exists' => 1
                ]);
            }
        }
    }

    private function get_headers_curl($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,            $url);
        curl_setopt($ch, CURLOPT_HEADER,         true);
        curl_setopt($ch, CURLOPT_NOBODY,         true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT,        15);

        $r = curl_exec($ch);
        //$r = explode("\n", $r);
        return $r;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('action', InputArgument::REQUIRED, 'Download or Rip'),
            array('lang', InputArgument::OPTIONAL, 'Habbo')
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

    private function jobRunning($name) {
        $ps = shell_exec("ps -aux | grep '".$name."' | grep -v 'grep' | grep -v '0:00'");
        if(strpos($ps, $name) !== false){
            echo "\n\nis Running!\n\n\n";
            var_dump($ps);
            return true;
        }else{
            return false;
        }
    }

    private function downloadNew ($lang = 'de')
    {

        if (!$this->jobRunning($this->name)) {
            echo 'Start' . "\n";
            $habbos = array(
                'com' => 'https://www.habbo.com/gamedata/external_flash_texts/0',
                'de' => 'https://www.habbo.de/gamedata/external_flash_texts/0',
                'tr' => 'https://www.habbo.com.tr/gamedata/external_flash_texts/0',
                'fi' => 'https://www.habbo.fi/gamedata/external_flash_texts/0',
                'es' => 'https://www.habbo.es/gamedata/external_flash_texts/0',
                'fr' => 'https://www.habbo.fr/gamedata/external_flash_texts/0',
                'it' => 'https://www.habbo.it/gamedata/external_flash_texts/0',
                'nl' => 'https://www.habbo.nl/gamedata/external_flash_texts/0',
                'br' => 'https://www.habbo.com.br/gamedata/external_flash_texts/0',
                'sandbox' => 'http://sandbox.habbo.com/gamedata/external_flash_texts/0'
            );

            $habbo = $habbos[$lang];
            $dataText = file_get_contents($habbo);

            $command = 'wget -q -O- '.$habbo.' | grep badge_name';
            $result = exec($command, $badges);

            foreach($badges as $badgeCode){
                preg_match("/(.*)_badge_name=(.*)|badge_name_(.*)=(.*)/mi", $badgeCode, $badgeData);
                if(count($badgeData) == 3 || count($badgeData) == 5) {
                    if(count($badgeData) == 5) {
                        list($foo, $foo, $foo, $badgeCode, $badgeName) = $badgeData;
                    }else{
                        list($foo, $badgeCode, $badgeName) = $badgeData;
                    }

                    if (!empty($badgeCode) && !empty($badgeName)) {

                        $badgeCode = trim($badgeCode);
                        $data = Badge::firstOrCreate(array('code' => $badgeCode));
                        $data->save();

                        /// ADD NAME
                        $name = DB::table('habbo_badges_name')
                            ->where('lang', '=', $lang)
                            ->where('badge_id', '=', $data->id)
                            ->orderBy('updated_at', 'DESC')
                            ->first();
                        if ($name == null || $name->name != $badgeName) {
                            DB::table('habbo_badges_name')->insert(
                                array(
                                    'lang' => $lang,
                                    'badge_id' => $data->id,
                                    'name' => $badgeName
                                )
                            );
                        } else {
                            DB::table('habbo_badges_name')
                                ->where('lang', '=', $lang)
                                ->where('badge_id', '=', $data->id)
                                ->update(
                                    array(
                                        'name' => $badgeName
                                    )
                                );
                        }

                        echo $badgeCode." name done; \n";
                    }
                }
            }

            $command = 'wget -q -O- '.$habbo.' | grep badge_desc';
            $result = exec($command, $badges);

            foreach($badges as $badgeCode) {
                preg_match("/(.*)_badge_desc=(.*)|badge_desc_(.*)=(.*)/mi", $badgeCode, $badgeData);
                if(count($badgeData) == 3 || count($badgeData) == 5) {
                    if(count($badgeData) == 5) {
                        list($foo, $foo, $foo, $badgeCode, $badgeDesc) = $badgeData;
                    }else{
                        list($foo, $badgeCode, $badgeDesc) = $badgeData;
                    }

                    if (!empty($badgeCode) && !empty($badgeDesc)) {

                        $badgeCode = trim($badgeCode);
                        $data = Badge::firstOrCreate(array('code' => $badgeCode));
                        $data->save();

                        $desc = DB::table('habbo_badges_description')
                            ->where('lang', '=', $lang)
                            ->where('badge_id', '=', $data->id)
                            ->orderBy('updated_at', 'DESC')
                            ->first();

                        if ($desc == null || $desc->description  != $badgeDesc) {
                            DB::table('habbo_badges_description')->insert(
                                array(
                                    'lang' => $lang,
                                    'badge_id' => $data->id,
                                    'description' => $badgeDesc
                                )
                            );
                        } else {
                            DB::table('habbo_badges_description')
                                ->where('lang', '=', $lang)
                                ->where('badge_id', '=', $data->id)
                                ->update(
                                    array(
                                        'description' => $badgeDesc
                                    )
                                );
                        }

                        echo $badgeCode." desc done; \n";

                    }
                }
            }
        }
    }

    private function download ($lang = 'de') {

        if(!$this->jobRunning($this->name)) {
            echo 'Start' . "\n";
            $habbos = array(
                'com' => 'https://www.habbo.com/gamedata/external_flash_texts/0',
                'de' => 'https://www.habbo.de/gamedata/external_flash_texts/0',
                'tr' => 'https://www.habbo.com.tr/gamedata/external_flash_texts/0',
                'fi' => 'https://www.habbo.fi/gamedata/external_flash_texts/0',
                'es' => 'https://www.habbo.es/gamedata/external_flash_texts/0',
                'fr' => 'https://www.habbo.fr/gamedata/external_flash_texts/0',
                'it' => 'https://www.habbo.it/gamedata/external_flash_texts/0',
                'nl' => 'https://www.habbo.nl/gamedata/external_flash_texts/0',
                'br' => 'https://www.habbo.com.br/gamedata/external_flash_texts/0',
                'sandbox' => 'http://sandbox.habbo.com/gamedata/external_flash_texts/0'
            );

            function remove_empty($array)
            {
                return array_filter($array, '_remove_empty_internal');
            }

            function _remove_empty_internal($value)
            {
                return !empty($value) || $value === 0;
            }

            $habbo = $habbos[$lang];

            //foreach ($habbos as $lang => $habbo) {
                echo $lang . "\n";
                $dataText = file_get_contents($habbo);

                preg_match_all(
                    "/(.*)_badge_name=(.*)|badge_name_(.*)=(.*)/mi",
                    $dataText,
                    $badges,
                    PREG_PATTERN_ORDER
                );

                $badges[1] = remove_empty($badges[1]);
                foreach ($badges[1] as $arrayId => $badgeCode) {

                    if (empty($badgeCode)) {
                        continue;
                    }

                    $badgeCode = trim($badgeCode);
                    $data = Badge::firstOrCreate(array('code' => $badgeCode));
                    $data->save();

                    /// ADD NAME
                    $name = DB::table('habbo_badges_name')
                        ->where('lang', '=', $lang)
                        ->where('badge_id', '=', $data->id)
                        ->first();
                    if ($name == null) {
                        DB::table('habbo_badges_name')->insert(
                            array(
                                'lang' => $lang,
                                'badge_id' => $data->id,
                                'name' => $badges[2][$arrayId]
                            )
                        );
                    } else {
                        DB::table('habbo_badges_name')
                            ->where('lang', '=', $lang)
                            ->where('badge_id', '=', $data->id)
                            ->update(
                                array(
                                    'name' => $badges[2][$arrayId]
                                )
                            );
                    }

                    //ADD DESC
                    preg_match(
                        "/" . $badgeCode . "_badge_desc=(.*)|badge_desc_" . $badgeCode . "=(.*)/mi",
                        $dataText,
                        $descString
                    );

                    if (count($descString)) {
                        $descString = $descString[count($descString) - 1];
                        if (!empty($descString)) {
                            $desc = DB::table('habbo_badges_description')
                                ->where('lang', '=', $lang)
                                ->where('badge_id', '=', $data->id)
                                ->first();
                            if ($desc == null) {
                                DB::table('habbo_badges_description')->insert(
                                    array(
                                        'lang' => $lang,
                                        'badge_id' => $data->id,
                                        'description' => $descString
                                    )
                                );
                            } else {
                                DB::table('habbo_badges_description')
                                    ->where('lang', '=', $lang)
                                    ->where('badge_id', '=', $data->id)
                                    ->update(
                                        array(
                                            'description' => $descString
                                        )
                                    );
                            }
                        }
                    }
                }

                //TOW
                $badges[3] = remove_empty($badges[3]);
                foreach ($badges[3] as $arrayId => $badgeCode) {
                    $badgeCode = trim($badgeCode);
                    $data = Badge::firstOrCreate(array('code' => $badgeCode));
                    $data->save();

                    /// ADD NAME
                    $name = DB::table('habbo_badges_name')
                        ->where('lang', '=', $lang)
                        ->where('badge_id', '=', $data->id)
                        ->first();
                    if ($name == null) {
                        DB::table('habbo_badges_name')->insert(
                            array(
                                'lang' => $lang,
                                'badge_id' => $data->id,
                                'name' => $badges[4][$arrayId]
                            )
                        );
                    } else {
                        DB::table('habbo_badges_name')
                            ->where('lang', '=', $lang)
                            ->where('badge_id', '=', $data->id)
                            ->update(
                                array(
                                    'name' => $badges[4][$arrayId]
                                )
                            );
                    }

                    //ADD DESC
                    preg_match(
                        "/" . $badgeCode . "_badge_desc=(.*)|badge_desc_" . $badgeCode . "=(.*)/mi",
                        $dataText,
                        $descString
                    );

                    if (count($descString)) {
                        $descString = $descString[count($descString) - 1];
                        if (!empty($descString)) {
                            $desc = DB::table('habbo_badges_description')
                                ->where('lang', '=', $lang)
                                ->where('badge_id', '=', $data->id)
                                ->first();
                            if ($desc == null) {
                                DB::table('habbo_badges_description')->insert(
                                    array(
                                        'lang' => $lang,
                                        'badge_id' => $data->id,
                                        'description' => $descString
                                    )
                                );
                            } else {
                                DB::table('habbo_badges_description')
                                    ->where('lang', '=', $lang)
                                    ->where('badge_id', '=', $data->id)
                                    ->update(
                                        array(
                                            'description' => $descString
                                        )
                                    );
                            }
                            $descString = null;
                        }
                    }
                }
            //}

            $badges = null;
            $dataText = null;
            unset($badges);
            unset($dataText);
        }
    }

    public function request ($url, $cookies) {

        $header = array();
        $header[] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5';
        $header[] = 'Cache-Control: max-age=0';
        $header[] = 'Connection: keep-alive';
        $header[] = 'Keep-Alive: 300';
        $header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';
        $header[] = 'Accept-Language: en-us,en;q=0.5';
        $header[] = 'Pragma: ';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729)');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_COOKIE, $cookies);
        return curl_exec($ch);
    }
}

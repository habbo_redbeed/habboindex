<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/api/habbo/motto/{lang}/{name}', 'ApiHabboController@mission')
    ->where('name', '.+');
Route::get('/api/habbo/{name}/{lang?}', 'ApiHabboController@get');
Route::get('/api/habbos', 'ApiHabboController@count');
Route::get('/api/habbos/list/{field}/{sort}', 'ApiHabboController@best');
Route::get('/api/habbos/list/{field}/{sort}/{start}', 'ApiHabboController@best');
Route::get('/api/habbos/list/{field}/{sort}/{start}/{lenght}', 'ApiHabboController@best');
Route::get('/api/habbos/list/{field}/{sort}/{start}/{lenght}/{lang}', 'ApiHabboController@best');

Route::get('/api/direct/habbo/{lang}', 'ApiHabboDirectController@get');
Route::get('/api/direct/habbo/{lang}/{name}', 'ApiHabboDirectController@get');

Route::get('/api/rooms', 'ApiRoomController@count');

Route::get('/api/images/{count?}', 'ApiImagesController@show')
    ->where('count', '[0-9]+');
Route::get('/api/images/search/{slug}/{count?}', 'ApiImagesController@find');
Route::any('/api/images/add', 'ApiImagesController@add');

Route::get('/api/badge/{badgecode}', 'ApiBadgeController@get');
Route::get('/api/badge/{name}/own', 'ApiBadgeController@ownIt');

Route::get('/api/badges', 'ApiBadgeController@count');
Route::get('/api/badges/langs', 'ApiBadgeController@langs');
Route::get('/api/badges/all', 'ApiBadgeController@all');
Route::get('/api/badges/week', 'ApiBadgeController@week');
Route::get('/api/badges/list', 'ApiBadgeController@all');
Route::get('/api/badges/last/{count}', 'ApiBadgeController@last');

Route::get('/api/badges/used/{sort?}/{lang?}', 'ApiBadgeController@used');

Route::get('/api/badges/not/{lang}', 'ApiBadgeController@allNot');
Route::get('/api/badges/{lang}', 'ApiBadgeController@all');
//Route::get('/api/badges/search/{search}', 'ApiBadgeController@find');

Route::get('/api/groups', 'ApiGroupController@count');

Route::get('/api/furnis', 'ApiController@count');
Route::get('/api/furni/search/{string}/', 'ApiController@search');
Route::get('/api/furni/last/{date}/', 'ApiController@last');
Route::get('/api/furni/since/{id}/', 'ApiController@sinceId');

Route::get('images/furni/{furni}.png', function ($furni) {
    $filepath = Cache::remember('funri_image_big_s' . $furni, (60 * 100), function () use ($furni) {
        $furni = DB::connection('mongodb')->table('furnis')->find($furni);
        if(isset($furni['furni']['image'])) {
            return $furni['furni']['image'];
        }else{
            if(isset($furni['animated']['clear']) && count($furni['animated']['clear'])) {
                $result = current($furni['animated']['clear']);
                if(is_array($result) && count($result)){
                    return current($result);
                }
                return current($furni['animated']['clear']);
            }else{
                return null;
            }

        }
    });

    return Response::download($filepath);

});

Route::get('images/furni/{furni}/direction/{direction}.png', function ($furni, $direction) {
    $filepath = Cache::remember('funri_image_direction_' . $furni . '_' . $direction, (60 * 100), function () use ($furni, $direction) {
        $furni = DB::connection('mongodb')->table('furnis')->find($furni);
        return $furni['logic']['directions'][$direction];
    });

    return Response::download($filepath);

});

Route::get('images/furni/{furni}/act/{direction}.png', function ($furni, $key) {
    $filepath = Cache::remember('funri_image_act_' . $furni . '_' . $key, (60 * 100), function () use ($furni, $key) {
        $furni = DB::connection('mongodb')->table('furnis')->find($furni);
        return $furni['logic']['click'][$key];
    });

    return Response::download($filepath);
});

Route::get('images/furni_icon/{furni}.png', function ($furni) {
    $filepath = Cache::remember('funri_image_icon_' . $furni, (60 * 100), function () use ($furni) {
        $furni = DB::connection('mongodb')->table('furnis')->find($furni);
        return $furni['furni']['icon_image'];
    });

    $dauer = 1200;
    $exp = gmdate("D, d M Y H:i:s", time() + (($dauer * 60) * 20));

    return Response::download($filepath, 'TEST.png', array('Expires' => $exp));
});

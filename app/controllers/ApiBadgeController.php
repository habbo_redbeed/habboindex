<?php

ini_set('memory_limit', '4096M');

class ApiBadgeController extends BaseController
{

    public function count()
    {
        $count = Cache::remember('badges_count', 60, function () {
            return Badge::count();
        });
        return Response::json(array('badges' => $count));
    }

    public function last($count)
    {
        $badges = [];
        $list = DB::table('habbo_badges')
        ->orderBy('created_at', 'ACS')
        ->take($count)
        ->get();

        foreach ($list as $badge) {
            $badgeArray = $this->formatBadge($badge);
            if($badgeArray) {
                $badges[] = $badgeArray;
            }
        }
        return $badges;

        return Response::json(array('badges' => $badges));
    }

    public function week()
    {
        $today = \Carbon\Carbon::now();
        $weekStart = \Carbon\Carbon::now()->startOfWeek();
        $weekStop = \Carbon\Carbon::now()->endOfWeek();

        $badges = Cache::remember('badge_week_'.$weekStart->format('Y-m-d H:i:s'), 50, function () use ($weekStart, $weekStop) {
            $list = DB::table('habbo_badges')
                    ->where('created_at', '>=', $weekStart->format('Y-m-d H:i:s'))
                    ->where('created_at', '<=', $weekStop->format('Y-m-d H:i:s'))
                    ->orderBy('created_at', 'DESC')
                    ->get();

            foreach ($list as $badge) {
                $badgeArray = $this->formatBadge($badge);
                if($badgeArray) {
                    $return[] = $badgeArray;
                }
            }
            return $return;
        });

        return Response::json(array('badges' => $badges));
    }

    public function used($sort = 'DESC', $lang = ''){

        $return = Cache::remember('badge__used_'.$sort.'_'.$lang, 360, function () use ($sort, $lang) {
            $habbos = DB::table('habbo_users')
                ->select(DB::raw('count(*) as habbos'), 'tld')
                ->groupBy('tld');

            $badges = DB::table('habbo_badge_used')
                ->select('habbo_badges.id as id', DB::raw('SUM(used) as useds'), 'habbo_badges.code as code', 'lang', 'habbo_badges.updated_at')
                ->join('habbo_badges', 'habbo_badges.id', '=', 'habbo_badge_used.badge_id');

            if ($lang) {
                $badges = $badges->where('lang', $lang);

                if ($lang == 'com') {
                    $lang = 'us';
                }

                $habbos = $habbos->where('tld', 'hh' . $lang);
            }

            $badges = $badges->groupBy('badge_id')
                ->groupBy('lang')
                ->orderBy('useds', $sort)
                ->get();

            $habbos = $habbos->lists('habbos', 'tld');
            //->get();

            $return = [];
            foreach ($badges as $badge) {


                $langs = $badge->lang;
                if ($langs == 'com') {
                    $langs = 'us';
                }

                if (isset($habbos['hh' . $langs])) {
                    $badgeArray = $this->formatBadge($badge);

                    if ($habbos['hh' . $langs] <= $badge->useds) {
                        $badgeArray['used'] = number_format(100, 2, '.', '');
                    } else {
                        $badgeArray['used'] = number_format(($badge->useds / $habbos['hh' . $langs]) * 100, 2, '.', '');
                    }
                    $return[$badge->code] = $badgeArray;
                }
            }

            return $return;
        });

        return Response::json($return);

    }

    private function formatBadge($badgeData)
    {

        return Cache::remember('badge_format_'.$badgeData->id.'_v2', 10, function () use ($badgeData) {

            $namesData = DB::table('habbo_badges_name')
                ->where('badge_id', '=', $badgeData->id)
                ->get();

    //        if(!isset($badgeData->code) || !$badgeData->code){
    //            $badgeData = Badge::find($badgeData->id);
    //        }

            if($badgeData->code || \Carbon\Carbon::parse($badgeData->notfound) <= \Carbon\Carbon::now()->subDay()) {
                $names = array();
                foreach ($namesData as $data) {
                    $names[$data->lang] = $data->name;
                }

                $namesData = DB::table('habbo_badges_description')
                    ->where('badge_id', '=', $badgeData->id)
                    ->get();

                $description = array();
                foreach ($namesData as $data) {
                    $description[$data->lang] = $data->description;
                }

                $img = 'http://images.habbo.com/c_images/album1584/' . $badgeData->code . '.gif';
                $return = array(
                    "image" => $img,
                    "code" => $badgeData->code,
                    'name' => $names,
                    'description' => $description,
                    'founded' => $badgeData->updated_at
                );

                return $return;

            }

            return null;
        });

    }

    public function langs() {
        $langs = DB::select("SELECT count(*) as count, lang FROM habbo_badges_name WHERE lang != 'sandbox' GROUP BY lang ORDER BY count(*) DESC");
        return Response::json($langs);
    }


    public function all($lang = '')
    {
        $keyAdd = '';
        $search = '';
        if(Input::has('s') && Input::get('s')){
            $search = Input::get('s');
            $search = rawurldecode($search);
            $keyAdd = '_'.md5($search).'_'.md5($lang);
        }else{
            $keyAdd = '_'.md5($lang);

        }
        $badges = Cache::remember('badge_list_all_v2'.$keyAdd, (60 * 3), function () use ($search, $lang) {
            $querys = [];
            if(empty($lang)) {
                $querys[] = "SELECT * FROM `habbo_badges` WHERE code LIKE '%" . $search . "%' AND habbo_badges.exists = 1";
            }

            $querys[] = "
              SELECT habbo_badges.*
              FROM habbo_badges
              LEFT JOIN habbo_badges_description
                ON (habbo_badges.id = habbo_badges_description.badge_id)

                WHERE (description LIKE '%".$search."%' OR habbo_badges.code LIKE '%" . $search . "%')
                AND habbo_badges.exists = 1
                AND habbo_badges_description.description != '' ".(!empty($lang) ? "AND lang = '".$lang."'" : '');

            $querys[] = "
              SELECT habbo_badges.*
              FROM habbo_badges
              LEFT JOIN habbo_badges_name
                ON (habbo_badges.id = habbo_badges_name.badge_id)
                WHERE (name LIKE '%".$search."%' OR habbo_badges.code LIKE '%" . $search . "%')
                AND habbo_badges.exists = 1
                AND habbo_badges_name.name != '' ".(!empty($lang) ? "AND lang = '".$lang."'" : '');


            $query = 'SELECT * FROM ('.implode(' UNION ', $querys).') as allBadges ORDER BY allBadges.code';
            $list = DB::select($query);
            $return = array();
            foreach ($list as $badge) {
                $badgeArray = $this->formatBadge($badge);
                if($badgeArray) {
                    $return[$badge->code] = $badgeArray;
                }
            }

            return $return;
        });
        return Response::json($badges);

    }

    public function allNot($lang)
    {
        $keyAdd = '';
        $search = '';
        if(Input::has('s') && Input::get('s')){
            $search = Input::get('s');
            $search = rawurldecode($search);
            $keyAdd = '_'.md5($search).'_'.md5($lang);
        }else{
            $keyAdd = '_'.md5($lang);

        }

        $badges = Cache::remember('badge_list_all_not-'.$keyAdd, (60 * 3), function () use ($search, $lang) {
            $querys = [];
            if(empty($lang)) {
                $querys[] = "SELECT * FROM `habbo_badges` WHERE code LIKE '%" . $search . "%' AND habbo_badges.exists = 1";
            }

            $querys[] = "SELECT habbo_badges.* FROM
                habbo_badges LEFT JOIN habbo_badges_description
                ON (habbo_badges.id = habbo_badges_description.badge_id)
                WHERE (description LIKE '%".$search."%'  OR habbo_badges.code LIKE '%".$search."%' )
                AND habbo_badges.exists = 1
                AND habbo_badges_description.description != '' AND lang != '".$lang."'
                AND '".$lang."' NOT IN(SELECT lang FROM habbo_badges_description WHERE badge_id = habbo_badges.id)
                AND '".$lang."' NOT IN(SELECT lang FROM habbo_badges_name WHERE badge_id = habbo_badges.id)
                GROUP BY habbo_badges.code";

            $querys[] = "SELECT habbo_badges.*
                  FROM habbo_badges
                  LEFT JOIN habbo_badges_name
                    ON (habbo_badges.id = habbo_badges_name.badge_id)
                    WHERE (name LIKE '%".$search."%'  OR habbo_badges.code LIKE '%".$search."%' )
                    AND habbo_badges.exists = 1
                AND habbo_badges_name.name != '' AND lang != '".$lang."'
                AND '".$lang."' NOT IN(SELECT lang FROM habbo_badges_name WHERE badge_id = habbo_badges.id)
                AND '".$lang."' NOT IN(SELECT lang FROM habbo_badges_description WHERE badge_id = habbo_badges.id)
                GROUP BY habbo_badges.code";


            $query = 'SELECT * FROM ('.implode(' UNION ', $querys).') as allBadges ORDER BY allBadges.code';
            $list = DB::select($query);
            $return = array();
            foreach ($list as $badge) {
                $badgeArray = $this->formatBadge($badge);
                if($badgeArray) {
                    $return[] = $badgeArray;
                }
            }

            return $return;
        });
        return Response::json($badges);

    }

    public function get($badgeCode)
    {
        $badge = DB::table('habbo_badges')
            ->where('code', '=', $badgeCode)
            ->first();

        $return = array(
            "image" => 'http://images.habbo.com/c_images/album1584/' . $badge['code'] . '.gif',
            "code" => $badge['code'],
            'name' => $badge['name']['com'],
            'description' => $badge['description']['com']
        );

        return Response::json($return);
    }

    public function find($search)
    {
        $list = DB::table('habbo_badges')
            ->where('code', 'regex', new MongoRegex("/.*" . $search . ".*/i"))
            ->orWhere(function ($query) use ($search) {
                $query->where('search', 'regex', new MongoRegex("/.*" . $search . ".*/i"));
            })
            ->take(200)
            ->get();

        $return = array();
        foreach ($list as $badge) {
            $return[] = array(
                "image" => 'http://images.habbo.com/c_images/album1584/' . $badge['code'] . '.gif',
                "code" => $badge['code'],
                'name' => $badge['name']['com'],
                'description' => $badge['description']['com']
            );
        }
        return Response::json($return);

    }

    public function ownIt($name, $all = false)
    {
        //var_dump($name); die;
        $habbos = Cache::remember('badge_ownIt_' . $name . '_ALL:' . ($all) ? 'YES' : 'NO', (-1), function () use ($name, $all) {
            $habbos = DB::table('habbo_users')
                ->where('badges', 'all', array($name));

            if ($all == false) {
                $habbos->take(500);
            }

            $habbos = $habbos->get(array('name'));

            $count = DB::table('habbo_users')
                ->where('badges', 'all', array($name))
                ->count();

            return array('count' => $count, 'habbo' => $habbos);
        });


        return Response::json($habbos);
    }

}
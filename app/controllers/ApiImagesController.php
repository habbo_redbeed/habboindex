<?php

ini_set('memory_limit', '4096M');

class ApiImagesController extends BaseController
{

    public function count()
    {
        $count = Cache::remember('habbo_images_count', 20, function () {
            return Badge::count();
        });
        return Response::json(array('badges' => $count));
    }


    public function show($count = 10)
    {
        $images = Cache::remember('habbo_images_'.$count, (20), function () use ($count) {
            $list = Images::take($count)
                ->orderBy('dt', 'desc')
                ->get();

            $return = array();
            foreach ($list as $img) {
                $return[] = $this->dump($img);
            }

            return $return;
        });
        return Response::json($images);

    }

    public function add()
    {

        $all = Input::all();
        $request = current($all);

        $jsonObject = json_decode($request);

        if(!isset($jsonObject->image) || empty($jsonObject->image)){
            return Response::json([
                'success' => false,
                'error' => 'Bild wurde nicht gefunden!',
                'field' => 'image',
                'request' => $jsonObject
            ]);
        }

        if(!isset($jsonObject->search) || empty($jsonObject->search)){
            return Response::json([
                'success' => false,
                'error' => 'Tags wurden nicht festgelegt!',
                'field' => 'search',
                'request' => $jsonObject
            ]);
        }

        $check = DB::connection('mongodb')
            ->table('habbo_images')
            ->where('src', '=', $jsonObject->image)
            ->count();

        if($check == 0){

            $image = array(
                'src' => $jsonObject->image,
                'search' => $jsonObject->search,
                "dt" => new MongoDate(strtotime(date("Y-m-d H:i:s")))
            );

            DB::connection('mongodb')
                ->table('habbo_images')
                ->insert($image);

            return Response::json([
                'success' => true,
                'error' => 'Bild hinzugefügt!',
                'field' => null
            ]);

        }else{
            return Response::json([
                'success' => false,
                'error' => 'Bild bereits hochgeladen!',
                'field' => 'image',
                'request' => $jsonObject
            ]);
        }
    }

    private function dump($img){
        $dump = array();

        $dump['img'] = $img->src;
        $dump['search'] = $img->search;
        $dump['found'] = (string)$img->dt;

        return $dump;
    }

    public function get($badgeCode)
    {
        $badge = DB::table('habbo_badges')
            ->where('code', '=', $badgeCode)
            ->orderBy('dt', 'desc')
            ->first();

        $return = array(
            "image" => 'http://images.habbo.com/c_images/album1584/' . $badge['code'] . '.gif',
            "code" => $badge['code'],
            'name' => $badge['name']['com'],
            'description' => $badge['description']['com']
        );

        return Response::json($return);
    }

    public function find($slug, $count = 10)
    {
        $images = Cache::remember('habbo_images_'.$count.'_'.md5($slug), (20), function () use ($count, $slug) {
            $regex = '/.*'.$slug.'.*/i';
            $list = Images::where('search', 'regexp', $regex)
                ->orderBy('dt', 'desc')
                ->take($count)
                ->get();

            $return = array();
            foreach ($list as $img) {
                $return[] = $this->dump($img);
            }

            return $return;
        });
        return Response::json($images);

    }



}
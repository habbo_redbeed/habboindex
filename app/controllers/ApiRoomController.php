<?php

ini_set('memory_limit', '4096M');

class ApiRoomController extends BaseController {
  
  public function count() {
    $count = Cache::remember('rooms_count', 60, function (){
      return Room::count();
    });
    return Response::json(array('rooms' => $count));
  }
  
}
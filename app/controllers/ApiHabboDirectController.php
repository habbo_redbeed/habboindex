<?php

ini_set('memory_limit', '4096M');

use Symfony\Component\Console\Output\BufferedOutput;

class ApiHabboDirectController extends BaseController
{

    /*
     |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */





    public function get($tld, $habboname = "")
    {

        if(empty($habboname)){
            $habboname = rawurlencode(Input::get('habbo'));
        }else{
            $habboname = rawurlencode($habboname);
        }
        if($tld == 'com'){
            $tld = 'us';
        }

        try {

            $json = Cache::remember('habbo_direct_'.$habboname.'_news_'.$tld, 45, function () use ($habboname, $tld) {

                $urlHeader = $this->getCookie();
                $habbo = $this->request('https://www.habbo.com/api/public/users?name=' .$habboname.'&site=hh'.$tld, $urlHeader);
                //$habbo = file_get_contents('http://www.habbo.com/api/public/users?name=' .$habboname.'&site=hh'.$tld, false, $urlHeader);


                if ($habbo != '') {
                    $json = json_decode($habbo, true);
                    //var_dump($json); die;
                    $habboId = $json['uniqueId']; //profileVisible

                    if ($habboId && $json['profileVisible']) {

                        //$profile = file_get_contents('https://www.habbo.com/api/public/users/' . $habboId . '/profile', false, $urlHeader);
                        $profile = $this->request('https://www.habbo.com/api/public/users/' . $habboId . '/profile', $urlHeader);
                        $json = json_decode($profile);

                        return $json;
                    }else{

                        return [
                            'user' => [
                                'selectedBadges' => $json['selectedBadges'],
                                'name' => $json['name'],
                                'motto' => $json['motto'],
                                'memberSince' => $json['memberSince']
                            ]
                        ];
                    }
                }

            });

            if($json){
                return Response::json($json);
            }

        } catch (Exception $e) {
            return Response::json(array(
                'success' => false,
                'error' => 'Habbo not found ',
                'hidden' => $e->getMessage(),
            ));
        }

        return Response::json(array(
            'success' => false,
            'error' => 'Habbo not found',
            'motto' => null,
        ));
    }

    private function getCookie($url = 'http://www.habbo.com/api/public/users?name=geschenketyb&site=hhus')
    {
        try {
            $header = array();
            $header[] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5';
            $header[] = 'Cache-Control: max-age=0';
            $header[] = 'Connection: keep-alive';
            $header[] = 'Keep-Alive: 300';
            $header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';
            $header[] = 'Accept-Language: en-us,en;q=0.5';
            $header[] = 'Pragma: ';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729)');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_AUTOREFERER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_ENCODING, '');
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            $getCookie = curl_exec($ch);
        }catch (Exception $e){
            //echo $e->getMessage(); die();
        }

        if (strpos($getCookie, 'setCookie') !== false) {
            $cookie = explode('setCookie(', $getCookie);
            foreach ($cookie as $key => $find) {
                if ($find{0} == "'" && strpos($find, 'document.referrer') === false) {
                    $cookie = $cookie[$key];
                }
            }

            $cookie = explode(');', $cookie);
            $cookie = current($cookie);
            $cookie = explode("',", $cookie);

            foreach ($cookie as &$value) {
                $value = trim($value);
                if ($value{0} == "'" || $value{0} == " ") {
                    $value = substr($value, 1);
                }
            }

            list($name, $value, $exp) = $cookie;


            return trim($name)."=".trim($value);

        } else {
            return $this->getCookie('https://www.habbo.com/stories-api/public/selfies');
        }

    }

    public function request ($url, $cookies) {

        $header = array();
        $header[] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5';
        $header[] = 'Cache-Control: max-age=0';
        $header[] = 'Connection: keep-alive';
        $header[] = 'Keep-Alive: 300';
        $header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';
        $header[] = 'Accept-Language: en-us,en;q=0.5';
        $header[] = 'Pragma: ';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729)');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_COOKIE, $cookies);
        return curl_exec($ch);
    }

}

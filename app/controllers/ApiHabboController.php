<?php

ini_set('memory_limit', '4096M');

use Symfony\Component\Console\Output\BufferedOutput;

class ApiHabboController extends BaseController
{

    /*
     |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function mission($lang, $habboname)
    {
        $apiHabbo = array('com', 'de', 'fr', 'fi', 'tr');
        //$habboname = urldecode($habboname);
        if (in_array($lang, $apiHabbo)) {
            return $this->missionApi($lang, $habboname);
        }else{
            return $this->missionWeb($lang, $habboname);
        }
    }


    public function missionWeb ($tld, $habboname){
        $baseUrl = 'http://www.habbo.'.$tld.'/home/';

        if($html && is_object($html)){
            $motto = $html->find('.profile-motto', 0);
            if($motto){
                $motto = $motto->innertext;
                if($motto){
                    return Response::json(array(
                        'success' => true,
                        'error' => null,
                        'motto' => trim(strip_tags((string) $motto)),
                    ));
                }
            }
        }

        return Response::json(array(
            'success' => false,
            'error' => 'Habbo not found',
            'motto' => null,
        ));
    }

    public function missionApi($tld, $habboname)
    {

        if($tld == 'com'){
            $tld = 'us';
        }

        $urlHeader = $this->getCookie();
        try {
            //$habbo = file_get_contents('http://www.habbo.com/api/public/users?name=' . $habboname.'&site=hh'.$tld, false, $urlHeader);

            $habboname = rawurlencode($habboname);
            $habbo = $this->request('http://www.habbo.com/api/public/users?name=' . $habboname.'&site=hh'.$tld, $urlHeader);
            if ($habbo != '') {
                $json = json_decode($habbo, true);
                $habboId = $json['uniqueId']; //profileVisible

                if ($habboId) {

//                    $profile = file_get_contents('https://www.habbo.com/api/public/users/' . $habboId . '/profile', false, $urlHeader);
                    $profile = $this->request('https://www.habbo.com/api/public/users/' . $habboId . '/profile', $urlHeader);
                    $json = json_decode($profile);

                    if (isset($json->user->motto)) {
                        return Response::json(array(
                            'success' => true,
                            'error' => null,
                            'motto' => trim(strip_tags((string) $json->user->motto)),
                        ));
                    }
                }
            }

        } catch (Exception $e) {
            return Response::json(array(
                'success' => false,
                'error' => 'Habbo not w found',
                'motto' => null,
                'debug' => $e->getmessage()
            ));
        }

        return Response::json(array(
            'success' => false,
            'error' => 'Habbo not s found',
            'motto' => null,
        ));
    }

    private function getCookie($url = 'http://www.habbo.com/api/public/users?name=bionissenDK&site=hhus')
    {
        try {
            $header = array();
            $header[] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5';
            $header[] = 'Cache-Control: max-age=0';
            $header[] = 'Connection: keep-alive';
            $header[] = 'Keep-Alive: 300';
            $header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';
            $header[] = 'Accept-Language: en-us,en;q=0.5';
            $header[] = 'Pragma: ';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729)');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_AUTOREFERER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_ENCODING, '');
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            $getCookie = curl_exec($ch);
        }catch (Exception $e){
            //echo $e->getMessage(); die();
        }

        if (strpos($getCookie, 'setCookie') !== false) {
            $cookie = explode('setCookie(', $getCookie);
            foreach ($cookie as $key => $find) {
                if ($find{0} == "'" && strpos($find, 'document.referrer') === false) {
                    $cookie = $cookie[$key];
                }
            }

            $cookie = explode(');', $cookie);
            $cookie = current($cookie);
            $cookie = explode("',", $cookie);

            foreach ($cookie as &$value) {
                $value = trim($value);
                if ($value{0} == "'" || $value{0} == " ") {
                    $value = substr($value, 1);
                }
            }

            list($name, $value, $exp) = $cookie;


            return trim($name)."=".trim($value);

//            $header = array();
//            $header[] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5';
//            $header[] = 'Cache-Control: max-age=0';
//            $header[] = 'Connection: keep-alive';
//            $header[] = 'Keep-Alive: 300';
//            $header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';
//            $header[] = 'Accept-Language: en-us,en;q=0.5';
//            $header[] = 'Pragma: ';
//            $header[] = "Cookie: ".trim($name)."=".trim($value);
//
//            $opts = array(
//                'http' => array(
//                    'method' => "GET",
////                    'header' => "Accept-language: en\r\n" . "Cookie: ".trim($name)."=".trim($value)."\r\n"."'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*\/*;q=0.8'"."\r\n",
//                    'timeout' => 30, // 1 200 Seconds = 20 Minutes
//                    'user_agent '  => "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2) Gecko/20100301 Ubuntu/9.10 (karmic) Firefox/3.6",
//                    'header' => $header
//                )
//            );

            return stream_context_create($opts);

        } else {
            return $this->getCookie('https://www.habbo.com/stories-api/public/selfies');
        }

    }

    public function count()
    {
        $count = Cache::remember('habbo_counts', 60, function () {
            return Habbo::count();
        });
        return Response::json(array('habbos' => $count));
    }

    public function get($name, $tld = 'hhde')
    {

        if(strpos($tld, 'hh') === false){
            $tld = 'hh'.$tld;
        }

        $habboData = Habbo::where('name', '=', $name)
            ->where('tld', $tld)
            ->first();

        if(!$habboData /*|| Carbon\Carbon::parse($habboData->created_at)->diffInDays() > 1*/) {

            $exec_string = 'php /var/www/habbo_cron/laravel/artisan ripper:habbo ripHabbo '.rawurlencode($name).' '.str_replace('hh', '', $tld).' > /dev/null &';
            exec($exec_string, $utput);
            if(!$habboData) {
                return Response::json(array('wait' => true, '$utput' => $utput, $exec_string));
            }

        }

        $habbo = new stdClass();
        $habbo->name = $habboData->name;
        $habbo->created = $habboData->habbo_created;
        $habbo->motto = $habboData->motto;

        if ($habbo != null) {

            if (isset($habboData->friends)) {
                $friendsList = array();
                foreach ($habboData->friends as $friend) {
                    $fData = Habbo::where('habbo_id', '=', $friend)->first();
                    if ($fData != null) {
                        $friendArray = array(
                            'api_id' => $fData->_id,
                            'name' => $fData->name
                        );
                        $friendsList[] = $friendArray;
                    }
                }
                $habbo->friends = $friendsList;
            }

            if ($habboData->rooms) {
                $roomList = array();
                foreach ($habboData->rooms as $key => $room) {
                    $roomData = $room->room;
                    $roomArray = array(
                        'api_id' => $roomData->_id,
                        'name' => $roomData->name
                    );
                    $roomList[] = $roomArray;
                }
                $habbo->rooms = $roomList;
            }


            if ($habboData->badges) {
                $badgeList = array();
                foreach ($habboData->badges as $key => $badge) {
                    $badgeData = $badge->badge;
                    $badgeArray = array(
                        'code' => $badgeData->code,
                        'name' => $badgeData->name,
                        'description' => $badgeData->description,
                    );
                    $badgeList[] = $badgeArray;
                }
                $habbo->badges = $badgeList;
            }
        }

        return Response::json($habbo);
    }


    public function best($field, $sort, $start = 0, $lenght = null, $lang = 'de')
    {
//     $cacheTime = -1;
        $cacheTime = 60;
        $key = 'habbos_best_' . $field . '_' . $sort . '_' . $start . '_' . $lenght.'_'.$lang;
        $counts = Cache::remember($key, $cacheTime, function () use ($sort, $field, $start, $lenght, $lang) {

            if($lang == 'com') {
                $lang = 'us';
            }

            if ($lenght == null) {
                $lenght = $start;
                $start = 0;
            }

            if ($lenght == 0) {
                $lenght = 50;
            }

            $habbos = HabboBadges::groupBy('habbo_users_'.$field.'.habbo_id')
                ->select('habbo_users_'.$field.'.habbo_id', DB::raw('count(*) as count'))
                ->join('habbo_users', 'habbo_users_'.$field.'.habbo_id', '=', 'habbo_users.id')
                ->orderBy(DB::raw('count(*)'), $sort)
                ->offset($start)
                ->take($lenght);

            $habbos = $habbos->where('habbo_users.tld', 'hh'.$lang);

            $habbos = $habbos->get('habbo_id');

            $return = array();

            foreach ($habbos as $habbo) {
                $habboData = $habbo->habbo;

                $habboArray = array(
                    "habbo_id" => $habbo['habbo_id'],
                    "name" => $habboData->name,
                    "count" => $habbo['count']
                );

                $return[] = $habboArray;
            }

            return $return;
        });

        Cache::forget($key);

        return Response::json($counts);
    }

    public function request ($url, $cookies) {

        $header = array();
        $header[] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5';
        $header[] = 'Cache-Control: max-age=0';
        $header[] = 'Connection: keep-alive';
        $header[] = 'Keep-Alive: 300';
        $header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';
        $header[] = 'Accept-Language: en-us,en;q=0.5';
        $header[] = 'Pragma: ';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729)');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_COOKIE, $cookies);
        return curl_exec($ch);
    }

}

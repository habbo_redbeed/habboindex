<?php

ini_set('memory_limit', '4096M');

class ApiGroupController extends BaseController {
  
  public function count() {
    $count = Cache::remember('group_count', 60, function (){
      return Group::count();
    });
    return Response::json(array('groups' => $count));
  }
  
}
<?php

class ApiController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function sinceId($id){

        $furni = DB::connection('mongodb')
            ->table('furnis')
            ->where('_id', '=', $id)
            ->whereNotNull('logic', '!=', null)
            ->orderBy('dt', 'DESC')
            ->first();

        $response = array();
        if($furni){

            $furnis = DB::connection('mongodb')
                ->table('furnis')
                ->where('dt', '>', $furni['dt'])
                ->whereNotNull('logic')
                ->orderBy('dt', 'desc')
                ->get();

            if($furnis) {

                $furnisArray = array();
                foreach ($furnis as $furni) {

                    if ((!isset($furni['furni']['image']) || !file_exists($furni['furni']['image']))) {
                        if (!isset($furni['logic']) || empty($furni['logic'])) {
                            continue;
                        }
                    }

                    if ($entry = $this->dumpFurni($furni)) {
                        if ($entry == null) {
                            continue;
                        }
                        $furnisArray[] = $entry;
                    }
                }

                $response['count'] = count($furnisArray);
                $response['furnis'] = $furnisArray;
            }

        }

        return Response::json($response);

    }

    public function last($date)
    {

        if ($date == 0) {
            $furnis = DB::connection('mongodb')->table('furnis')->get();
        } else if (is_numeric($date) == false) {
            $furnis = DB::connection('mongodb')->table('furnis')
                ->where('dt', '>=', new MongoDate(strtotime($date)))
                ->where('dt', '<', new MongoDate(strtotime($date . ' + 1 day')))
                ->get();
        } else {
            $furnis = DB::connection('mongodb')->table('furnis')
                ->orderBy('dt', 'desc')
                ->take($date)
                ->whereNotNull('logic')
                ->get();
        }

        $response = array();

        foreach ($furnis as $furni) {

            if((!isset($furni['furni']['image']) || !file_exists($furni['furni']['image']))){
                if(!isset($furni['logic']) || empty($furni['logic'])){
                    continue;
                }
            }

            if($entry = $this->dumpFurni($furni)){
                if($entry == null){
                    continue;
                }
                $response[] = $entry;
            }
        }

        return Response::json(array($response));
    }

    public function search($string)
    {

        if(!empty($string)) {
            $furnis = DB::connection('mongodb')->table('furnis')
                ->where('furni.name', 'regex', new MongoRegex("/.*".$string.".*/i"))
                ->orWhere('furni.description', 'regex', new MongoRegex("/.*".$string.".*/i"))
                ->orWhere('swf.name', 'regex', new MongoRegex("/.*".$string.".*/i"))
                ->orWhere('swf.sprite_id', '=', $string)
                ->orWhere('revision', '=', $string)
                ->orWhere('swf.name', 'regex', new MongoRegex("/.*".$string.".*/i"))
                ->orderBy('dt', 'DESC')
                ->take(20)
                ->remember(360)
                ->get();
        }else{
            return $this->last(20);
        }

        $response = array();

        foreach ($furnis as $furni) {


            if(!isset($furni['furni']['image']) || !file_exists($furni['furni']['image'])){
                continue;
            }

            if($entry = $this->dumpFurni($furni)){
                if($entry == null){
                    continue;
                }
                $response[] = $entry;
            }
        }

        return Response::json(array($response));
    }

    private function dumpFurni ($furni) {

        $entry = new stdClass();
        $entry->name = $furni['furni']['name'];
        $entry->uid = (string)$furni['_id'];
        $entry->description = $furni['furni']['description'];

        $entry->image = 'http://api.habbos.redbeed.de/images/furni/' . $furni['_id'] . '.png';
        $entry->icon = 'http://api.habbos.redbeed.de/images/furni_icon/' . $furni['_id'] . '.png';
        if(isset($furni['dt'])) {
            $entry->dt = date('Y-M-d H:i:s', $furni['dt']->sec);
        }else{
            $entry->dt = null;
        }

        if(isset($furni['logic'])) {
            $directions = array();
            foreach ($furni['logic']['directions'] as $direction => $image) {
                if(strpos($image, 'http') === false) {
                    if (!file_exists($image)) {
                        continue;
                    }
                    $directions[$direction] = 'http://api.habbos.redbeed.de/images/furni/' . $furni['_id'] . '/direction/' . $direction . '.png';
                }else{
                    $directions[$direction] = $image;
                }
            }

            if(count($directions) < 1){
                $directions = array(
                    '0' => $entry->image
                );
            }

            $click = array();
            foreach ($furni['logic']['click'] as $key => $image) {
                if(strpos($image, 'http') === false) {
                    if (!file_exists($image)) {
                        continue;
                    }
                    $click[$key] = 'http://api.habbos.redbeed.de/images/furni/' . $furni['_id'] . '/act/' . $key . '.png';
                }else{
                    $click[$key] = $image;
                }
            }
        }else{
            return null;
        }

        $entry->direction = $directions;
        $entry->act = $click;

        return $entry;


    }

    public function count()
    {
        $count = Cache::remember('furnis_count', 60, function () {
            return DB::connection('mongodb')->table('furnis')->count();
        });
        return Response::json(array('furnis' => $count));
    }

}

<?php

class HabboBadges extends Eloquent {

    protected $table = 'habbo_users_badges';

    protected $fillable = array(
        'habbo_id', 'badge_id'
    );

    public function badge() {
        return $this->hasOne('Badge', 'id', 'badge_id');
    }

    public function habbo() {
        return $this->hasOne('Habbo', 'id', 'habbo_id');
    }

}

?>
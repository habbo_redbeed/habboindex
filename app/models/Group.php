<?php

class Group extends Eloquent {

  protected $table = 'habbo_groups';
  
  protected $fillable = array(
      'group_id',
      'tld',
      'name',
      'description',
      'primaryColour',
      'secondaryColour',
      'badgeCode',
      'room_id',
      'type',
  );

}

?>
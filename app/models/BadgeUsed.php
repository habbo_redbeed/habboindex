<?php

class BadgeUsed extends Eloquent {

    protected $table = 'habbo_badge_used';

    protected $fillable = array(
        'id', 'badge_id', 'lang', 'used'
    );

}

?>
<?php

class HabboGroups extends Eloquent {

    protected $table = 'habbo_users_groups';

    protected $fillable = array(
        'habbo_id', 'group_id', 'is_admin'
    );

    public function group() {
        return $this->hasOne('Badge', 'id', 'group_id');
    }

    public function habbo() {
        return $this->hasOne('Habbo', 'id', 'habbo_id');
    }

}

?>
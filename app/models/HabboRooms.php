<?php

class HabboRooms extends Eloquent {

    protected $table = 'habbo_users_rooms';

    protected $fillable = array(
        'habbo_id', 'room_id'
    );

    public function room() {
        return $this->hasOne('Room', 'id', 'room_id');
    }

    public function habbo() {
        return $this->hasOne('Habbo', 'id', 'habbo_id');
    }

}

?>
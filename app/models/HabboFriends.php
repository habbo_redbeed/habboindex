<?php

class HabboFriends extends Eloquent {

    protected $table = 'habbo_users_friends';

    protected $fillable = array(
        'habbo_id', 'friend_id'
    );

}

?>
<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Jenssegers\Mongodb\Model as Eloquent;

class Images extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'habbo_images';
    protected $connection = 'mongodb';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

}

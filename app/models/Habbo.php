<?php

class Habbo extends Eloquent {

  protected $table = 'habbo_users';
  
  protected $fillable = array(
      'id',
      'habbo_id', 
      'name',
      'habbo_created',
      'motto',
      'last_rip',
      'tld'
      
  );

    public function badges () {
        return $this->hasMany('HabboBadges');
    }

    public function rooms () {
        return $this->hasMany('HabboRooms');
    }

    public function groups () {
        return $this->hasMany('HabboGroups');
    }
//
//    public function friends () {
//        return $this->hasMany('HabboFriends');
//    }

}

?>
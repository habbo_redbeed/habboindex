<?php


class Room extends Eloquent {

  protected $table = 'habbo_rooms';
  
  protected $fillable = array(
      'room_id',
      'tld',
      'name',
      'description',
      'habbo_own',
  );

}

?>